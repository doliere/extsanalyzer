/*
  
A tool for analyzing the message passing APIs 
between browser extensions and web applications
   
   Copyright (C) 2017 - 2019 INRIA
                                                                     
    Bug descriptions, user reports, comments or suggestions are   
    welcome. Send them to doliere.some@inria.fr or some.frncs@gmail.com
                                   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or 
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,   
   but WITHOUT ANY WARRANTY; without even the implied warranty of    
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     
   GNU General Public License for more details.                      
                                                                     
   You should have received a copy of the GNU General Public         
   License along with this program; if not, write to the Free        
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,   
   MA 02111-1307, USA.  

*/
// =======================================================================================
//    asthandler.js
//    ------------------------------------------------------------------------------------           
//    Author      :  Dolière Francis Somé                                    
//    Creation    :  Monday January 21 11:04:40 2019                          
//    Last change :  Monday January 21 11:04:40 2019               
//    Copyright   :  2017-2019 Dolière Francis Somé                          
// =======================================================================================


const recast = require('recast')


let assignments = {},
	declarations = {},
	constructs = {}

	//Get function name and arguments
function getFunctionNameAndArguments(fcallee, fargs, cons){
	if(cons)
		constructs = cons
	let isBind = false
	if(fcallee.type == "MemberExpression"){
		if(handleObjectProperty(fcallee, "bind", cons)){
			//return [fcallee]

			fargs = fargs.length > 0 ? fargs.slice(1) : fargs
			fcallee = fcallee.object
			isBind = true
		}
		if(handleObjectProperty(fcallee, "call", cons) || 
			handleObjectProperty(fcallee, "apply", cons)){
			fcallee = fcallee.object
			fargs = fargs.length > 0 ? fargs.slice(1) : fargs
		}
	}
	while(fcallee.type == "CallExpression" && 
			fcallee.callee.type == "MemberExpression" &&
		 handleObjectProperty(fcallee.callee, "bind", cons)){
		fargs = fcallee.arguments.slice(1).concat(fargs)
		fcallee = fcallee.callee.object
	}
	return [fcallee, fargs, isBind]
}

	//Handle Object Property
function handleObjectProperty(node, search, cons){
	if(cons)
		constructs = cons
	if(node.type == "MemberExpression"){
		let prop = node.property,
			computed = node.computed,
			ptype = prop.type,
			pvalue = ptype == "Literal" ? prop.value : prop.name
		if((ptype == "Literal" && computed == true && pvalue == search) ||
			(ptype == "Identifier" && computed == false && pvalue == search) ||
			(ptype != "Literal" && computed == true && resolveExpression(prop, search, cons))) {
			return true
		}
	}
	return false
}

	//Resolve an expression
function resolveExpression(expression, search, cons){
	if(cons)
		constructs = cons
	let values = [],
		types = {"Identifier": "", "MemberExpression": ""}
	let left, right, nvals, als, lvals, rvals

		//If we have  string, just compare it to search
	if (typeof expression == "string")
		return expression.toLowerCase() == search.toLowerCase()

	switch(expression.type){
		case "Literal":
			if(typeof expression == "string")
				return expression.value.toLowerCase() == search.toLowerCase()
			else
				return expression.value == search
			break;
		case "Identifier":
				//Get literals of the other identifier
			//nvals = lookupNonAliasingsOfType(expression, cons, "Literal")
			nvals = getLiteralsAssignments(expression, cons)

			for(let i=0; i<nvals.length; i++){
				if(nvals[i].value == search)
					return true
			}
			/*	//Get aliases
			als = findAliases(expression, cons)
			//Get only their literals
			if(als){
				for(let al in als){
					let anvals = lookupNonAliasingsOfType(al, cons, "Literal")
					for(let i=0; i<anvals.length; i++){
						if(anvals[i].value == search)
							return true
					}
				}
			}*/
			break
		case "LogicalExpression": // Involving only 2 operands
				left = expression.left,
				right = expression.right

				//Lookup Left
			if(left.type == "Literal")
				lvals = [left]
			else if(left.type in types){
				//lvals = lookupNonAliasingsOfType(left, cons, "Literal")
				lvals = getLiteralsAssignments(expression, cons)
			}else{
				lvals = []
			}

				//Lookup Right
			if(right.type == "Literal")
				rvals = [right]
			else if(right.type in types){
				//rvals = lookupNonAliasingsOfType(right, cons, "Literal")
				rvals = getLiteralsAssignments(expression, cons)
			}else{
				rvals = []
			}
			
			if(expression.operator == "&&"){
				for(let r=0; r<rvals.length; r++){
					if(rvals[r].value == search)
						return true
				}
				return false
			}else if(expression.operator == "||"){
					//Left, 
				for(let l=0; l<lvals.length; l++){
					if(lvals[l].value == search)
						return true
				}
					//Or right
				for(let r=0; r<rvals.length; r++){
					if(rvals[r].value == search)
						return true
				}
				return false
			}else{
				return false // We do not know how to handle such operator
			}
			break
		case "BinaryExpression":
		 	left = expression.left,
			right = expression.right
				//Lookup Left
			if(left.type == "Literal")
				lvals = [left]
			else if(left.type in types){
				//lvals = lookupNonAliasingsOfType(left, cons, "Literal"),
				lvals = getLiteralsAssignments(expression, cons)
			}else{
				lvals = []
			}

				//Lookup Right
			if(right.type == "Literal"){
				rvals = [right]
			}else if(right.type in types){
				//rvals = lookupNonAliasingsOfType(right, cons, "Literal")
				rvals = getLiteralsAssignments(expression, cons)
			}else{
				rvals = []
			}

			if(expression.operator == "+"){
				for(let l=0; l<lvals.length; l++){
					for(let r=0; r<rvals.length; r++){
						if(lvals[l].value + rvals[r].value == search)
							return true
					}
				}
			}
			return false
			break
		case "AssignmentExpression":
			return resolveExpression(expression.left, search, cons)
			break
		case "CallExpression":
			//Try to eval, and if literal, return 
			try{	
				let res = eval(recast.prettyPrint(expression).code)
				return res == search				
			}catch(e){
				return false
			}
			break
		default:
			return expression === search
			break
	}
	return false
}


	//Resolve expression of a certain type
function resolveExpressionOfType(expression, cons, type){
	if(cons){
		constructs = cons
	}
	
	let results = []
	switch(expression.type){
		case type:
			results.push(expression)
			break;
		case "Identifier":
			results = getAssignmentsOfType(expression, cons, type)
			//If aliases, get their functions as well
			break;
		case "MemberExpression":
			results = getAssignmentsOfType(expression, cons, type)
			break;
		case "CallExpression":
			let res = resolveCallExpression(expression, cons)
			if(res){
				if(res.type == type){
					results.push(res)
					break
				}else if(res.type == "Identifier" || res.type == "MemberExpression"){
					results = resolveExpressionOfType(res, cons, type)
				}
			}
			break
		case "LogicalExpression": // Involving only 2 operands
			let left = expression.left,
				right = expression.right
			if(expression.operator == "&&"){
				results = resolveExpressionOfType(right, cons, type)
			}else if(expression.operator == "||"){
				let lres = resolveExpressionOfType(left, cons, type),
					rres = resolveExpressionOfType(right, cons, type)
				if(lres)
					results = results.concat(lres)
				if(rres)
					results = results.concat(rres)
			}//Else, we do not know how to handle this operator
			break
		
		case "AssignmentExpression":
			results = results.concat(resolveExpressionOfType(expression.left, cons, type))
			break
		default:
			break
	}
	return results
}

	//Get left values of
function lookupNonAliasingsOfType(left, cons, type){
	if(cons)
		constructs = cons
	let results = [],
		lcast = typeof left == "string" ? left : recast.prettyPrint(left).code,
		aindex = constructs.aindex

	if((lcast in aindex) && (type in aindex[lcast])) {
		results = aindex[lcast][type]
	}
	return results
}



	//An identifier, member expression is assigned to an identifier or another member expression, recursively
function findAliases(left, cons){
	if(cons)
		constructs = cons
	let lcast = typeof left == "string" ? left : recast.prettyPrint(left).code,
		aliases = constructs.aliases,
		results = {}

	if((lcast in aliases)) {
		results = aliases[lcast]
	}
	return results
}


	//Variables are assigned literals
function getLiteralsAssignments(left, cons){
	if(cons)
		constructs = cons
	let results = []
		lcast = typeof left == "string" ? left : recast.prettyPrint(left).code,
		aindex = constructs.aindex,
		aliases = constructs.aliases

	//console.log("ALS ", Object.keys(constructs))

	let ids = {}
		ids[lcast] = ""
	if(aliases){
		if( (lcast in aliases) ){
			ids = Object.assign({}, ids, aliases[lcast])
		}
	}
		//Get assignments 
	if(aindex){
		for(let id in ids){
			lcast = id
			if((lcast in aindex)) {
				if(aindex[lcast].Literal)
					results = results.concat(aindex[lcast].Literal)
			}
		}
	}

	return results
}

	//Variables are assigned a certain type
function getAssignmentsOfType(left, cons, type){
	if(cons)
		constructs = cons
	let results = []
		lcast = (typeof left == "string") ? left : recast.prettyPrint(left).code,
		aindex = constructs.aindex,
		aliases = constructs.aliases

	let ids = {}
		ids[lcast] = ""
	if( (lcast in aliases) ){
		ids = Object.assign({}, ids, aliases[lcast])
	}

		//Get assignments 
	for(let id in ids){
		lcast = id
		if((lcast in aindex)) {
			if(aindex[lcast][type])
				results = results.concat(aindex[lcast][type])
		}
	}

	return results
}


	//Variables are assigned functions
function getFunctionsAssignments(left, cons){
	if(cons)
		constructs = cons
	let results = []
		lcast = typeof left == "string" ? left : recast.prettyPrint(left).code,
		aindex = constructs.aindex,
		aliases = constructs.aliases

	let ids = {}
		ids[lcast] = ""
	if( (lcast in aliases) ){
		ids = Object.assign({}, ids, aliases[lcast])
	}

		//Get assignments 
	for(let id in ids){
		lcast = id
		if((lcast in aindex)) {
			if(aindex[lcast].FunctionExpression)
				results = results.concat(aindex[lcast].FunctionExpression)
			if(aindex[lcast].FunctionDeclaration)
				results = results.concat(aindex[lcast].FunctionDeclaration)
			if(aindex[lcast].ArrowFunctionExpression)
				results = results.concat(aindex[lcast].ArrowFunctionExpression)

		}
	}
	return results
}



	//Variables are assigned object expressions
function getObjectExpressionsAssignments(left, cons){
	if(cons)
		constructs = cons
	let results = []
		lcast = typeof left == "string" ? left : recast.prettyPrint(left).code,
		aindex = constructs.aindex,
		aliases = constructs.aliases

	let ids = {}
		ids[lcast] = ""
	if( (lcast in aliases) ){
		ids = Object.assign({}, ids, aliases[lcast])
	}

		//Get assignments 
	for(let id in ids){
		lcast = id
		if((lcast in aindex)) {
			if(aindex[lcast].ObjectExpression)
				results = results.concat(aindex[lcast].ObjectExpression)
		}
	}

	return results
}


	//Resolve an expression
	//1. Directly function expression
	//2. Identifier or member expression...
	//3. Function call, which is a bind...Fallback to 2
	//4. Function call which returns a function
	//console.log("Resolving Function")
function resolveFunction(expression, cons){
	if(cons){
		constructs = cons
	}
	
	let results = []
	switch(expression.type){
		case "FunctionExpression":
			results.push(expression)
			break;
		case "FunctionDeclaration":
			results.push(expression)
			break
		case "ArrowFunctionExpression":
			results.push(expression)
			break
		case "Identifier":
			results = getFunctionsAssignments(expression, cons)
			break;
		case "MemberExpression":
			results = getFunctionsAssignments(expression, cons)
			break;
		case "CallExpression":
			let res = resolveCallExpression(expression, cons)
			if(res){
				if(res.type == "FunctionExpression" || 
					res.type == "FunctionDeclaration" || 
					res.type == "ArrowFunctionExpression"){
					results.push(res)
					break
				}else if(res.type == "Identifier" || res.type == "MemberExpression"){
					results = resolveFunction(res, cons)
				}else{
					//We have a literal here
				}
			}
			break
		case "LogicalExpression": // Involving only 2 operands
			let left = expression.left,
				right = expression.right
			if(expression.operator == "&&"){
				results = resolveFunction(right, cons)
			}else if(expression.operator == "||"){
				let lres = resolveFunction(left, cons),
					rres = resolveFunction(right, cons)
				if(lres)
					results = results.concat(lres)
				if(rres)
					results = results.concat(rres)
			}//Else, we do not know how to handle this operator
			break
		
		case "AssignmentExpression":
			results = results.concat(resolveFunction(expression.left, cons))
			break
		default:
			break
	}
	return results
}



	//Resolve object expressions
function resolveObjectExpression(expression, cons){
	if(cons){
		constructs = cons
	}
	
	let results = []
	switch(expression.type){
		case "ObjectExpression":
			results.push(expression)
			break;
		case "Identifier":
			results = getObjectExpressionsAssignments(expression, cons)
			//If aliases, get their functions as well
			break;
		case "MemberExpression":
			results = getObjectExpressionsAssignments(expression, cons)
			break;
		case "CallExpression":
			let res = resolveCallExpression(expression, cons)
			if(res){
				if(res.type == "ObjectExpression"){
					results.push(res)
					break
				}else if(res.type == "Identifier" || res.type == "MemberExpression"){
					results = resolveObjectExpression(res, cons)
				}else{
					//We have a literal here
				}
			}
			break
		case "LogicalExpression": // Involving only 2 operands
			let left = expression.left,
				right = expression.right
			if(expression.operator == "&&"){
				results = resolveObjectExpression(right, cons)
			}else if(expression.operator == "||"){
				let lres = resolveObjectExpression(left, cons),
					rres = resolveObjectExpression(right, cons)
				if(lres)
					results = results.concat(lres)
				if(rres)
					results = results.concat(rres)
			}//Else, we do not know how to handle this operator
			break
		
		case "AssignmentExpression":
			results = results.concat(resolveObjectExpression(expression.left, cons))
			break
		default:
			break
	}
	return results
}





//Try to resolve Call Expression
	/**
	*** 1. FunctionExpression/FunctionDeclaration
	*** 2. Literal
	*** 3. Identifier (function aliasing)
	**/
function resolveCallExpression(expression, cons){
	if(cons){
		constructs = cons
	}
	let result = null
	if(expression.type == "CallExpression"){
		//If this a bind call, it is a function aliasing
		try{
			//We try to evaluate a function here
			if(expression.callee.type != "FunctionExpression" &&
				expression.callee.type != "FunctionDeclaration" && 
				expression.callee.type != "ArrowFunctionExpression"){
				throw Error("Not the right type")
			}
			let feval = eval(recast.prettyPrint(expression).code)
			
			if(typeof feval == "function"){
				//parse feval.toString()
				//
				let fname = feval.name || ''
					fname = fname.split(' ')
				if(fname.length > 1){ // Bound Function
					result = parseExpression(fname[1])
				}else{ // Normal function
					result = parseExpression(feval.toString())
				}
			}else{
				//Another expression // Literal expression
				result = parseExpression(feval.toString())
				if(result.type == "Identifier"){
					result.type = "Literal"
					result.value = result.name
					result.raw = '"'+ result.name +'"'
					delete result.name
				}
			}
			
		}catch(e){
			//We have no function expression, or function expression that did not return results
			//Only Bound Function
			if(expression.callee && 
				(expression.callee.type != "FunctionDeclaration") &&
				(expression.callee.type != "FunctionExpression") && 
				(expression.callee.type != "ArrowFunctionDeclaration")){
				
				//Is a bound expression
				let res = getFunctionNameAndArguments(expression.callee, expression.arguments, cons)
				if(res)
					result = parseExpression(res[0])
				//Can be only bound function
			}
			result = null // Did not know how to handle this
		}
	}
	return result
}


	//Parse and return expression
function parseExpression(code){
	try{
		let ast = recast.parse("(" + code + ")")
		return ast.program.body[0].expression
	}catch(e){}
	return null
}

module.exports = {
	getFunctionNameAndArguments: getFunctionNameAndArguments,
	findAliases: findAliases,
	lookupNonAliasingsOfType: lookupNonAliasingsOfType,
	resolveExpression: resolveExpression,
	resolveExpressionOfType: resolveExpressionOfType,
	resolveFunction: resolveFunction,
	resolveObjectExpression: resolveObjectExpression, 
	resolveCallExpression: resolveCallExpression,
	getLiteralsAssignments: getLiteralsAssignments,
	getAssignmentsOfType: getAssignmentsOfType,
	getObjectExpressionsAssignments: getObjectExpressionsAssignments
}