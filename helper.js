/*
  
A tool for analyzing the message passing APIs 
between browser extensions and web applications
   
   Copyright (C) 2017 - 2019 INRIA
                                                                     
    Bug descriptions, user reports, comments or suggestions are   
    welcome. Send them to doliere.some@inria.fr or some.frncs@gmail.com
                                   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or 
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,   
   but WITHOUT ANY WARRANTY; without even the implied warranty of    
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     
   GNU General Public License for more details.                      
                                                                     
   You should have received a copy of the GNU General Public         
   License along with this program; if not, write to the Free        
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,   
   MA 02111-1307, USA.  

*/
// =======================================================================================
//    helper.js
//    ------------------------------------------------------------------------------------           
//    Author      :  Dolière Francis Somé                                    
//    Creation    :  Monday January 21 11:04:40 2019                          
//    Last change :  Monday January 21 11:04:40 2019               
//    Copyright   :  2017-2019 Dolière Francis Somé                          
// =======================================================================================


const fs = require('fs'),
	path = require('path'),
	recast = require('recast'),
	rewrite = require('./rewrite'),
	targetter = require('./targets'),
	mlisteners = require('./messagelisteners'),
	astvisit = require('./astvisit'),
	asthandler = require('./asthandler'),
	reader = require('./reader')

	//Analyse listeners of page messages
function pageMessagesCScripts(constructs){
	//Gather targets...
	let targets = targetter.prepareTargets(),
		/*addListeners = mlisteners.getWindowMessageListeners(constructs, targets.domtargets),
		onmessages = mlisteners.getWindowMessageListenersAssignment(constructs, targets.domtargets2),
		handlers = []
	if(addListeners)
		handlers = handlers.concat(addListeners)
	if(onmessages)
		handlers = handlers.concat(onmessages)
	*/
		[ahandlers, amatches] = mlisteners.getWindowMessageListeners(constructs, targets.domtargets),
		[ohandlers, omatches] = mlisteners.getWindowMessageListenersAssignment(constructs, targets.domtargets2)
	
	
	return [ahandlers.concat(ohandlers), Object.assign({}, amatches, omatches)]
	//return handlers
}
	//ALL Post messages calls
function postMessageCalls(constructs){
	let targets = targetter.prepareTargets()
	return mlisteners.getMatchedFunctionsNbArguments(constructs, targets.postMessages)
}

	//All Extensions Content Injection Calls
function extensionContentInjection(constructs){
	let targets = targetter.prepareTargets()
	return cinjection.getInjectedContentURLs(constructs, targets.appendChild, targets.getURL)
}
	
	//Return port listeners, in different parts of the extension...
function getPortListeners(constructs){
	let targets = targetter.prepareTargets(),
		aports = mlisteners.getExtensionConnectAssigner(constructs, targets.ctargets), // port = runtime.connect()
		oports = mlisteners.getExtensionConnectPorts(constructs, targets.etargets_c), // onConnect(function(port){})
		extports = mlisteners.getExtensionConnectPorts(constructs, targets.extargets_c), // onConnectExternal(function(port){})

		[ahandlers, amatches] = getAssignmentPortHandlers(aports, constructs),
	 	[ohandlers, omatches] = getConnectPortsHandlers(oports),
		[ehandlers, ematches] = getConnectPortsHandlers(extports)

	return [{
		"extension": ahandlers.concat(ohandlers),
		"external": ehandlers,
	}, {
		"aports": Object.keys(aports),
		"oports": Object.keys(oports),
		"extports": Object.keys(extports)
	}, {
		"amaches": amatches,
		"omatches": omatches,
		"ehandlers": ehandlers
	}]
}
	//Get Assignment port listeners
function getAssignmentPortHandlers(ports, constructs){
	let targets = [],
		prefs = targetter.prefs,
		esuffix = targetter.esuffix
		//Generate dynamic targets
	for(let port in ports){
		let dtargets = targetter.dynamicTargets(port +  esuffix, prefs)
		if(dtargets)
			targets = targets.concat(dtargets)
	}
		//Get handlers...
	return mlisteners.getExtensionMessageListeners(constructs, targets)
}
	
	//Get ConnectPorts Handlers
function getConnectPortsHandlers(ports){
	let prefs = targetter.prefs,
		esuffix = targetter.esuffix,
		handlers = [],
		matches = {}
	for(let port in ports){
		//Generate targets
		let targets = targetter.dynamicTargets(port + esuffix, prefs)
		for(let p=0; p<ports[port].length; p++){
			let body = ports[port][p]
			//Get constructs corresponding to body...
			let bconstructs = astvisit.initiateVisits(body)[0]
			if(bconstructs){
				let [lhandlers, lmatches] = mlisteners.getExtensionMessageListeners(bconstructs, targets)
				if(lhandlers)
					handlers = handlers.concat(lhandlers)
				if(lmatches)
					matches = Object.assign({}, matches, lmatches)
			}
		} 
	}
	return [handlers, matches]
}

	// Extension Messages
function getExtensionListeners(constructs){
	let targets = targetter.prepareTargets(),
		elisteners = mlisteners.getExtensionMessageListeners(constructs, targets.etargets)
	//console.log("Extension Listeners ", elisteners)
	return elisteners
}
	
	// Get request and response header listeners
function getRequestsListeners(constructs){
	let targets = targetter.prepareTargets()
	return mlisteners.getExtensionMessageListeners(constructs, targets.requests)
}

	// Get request and response header listeners
function getResponsesListeners(constructs){
	let targets = targetter.prepareTargets()
	return mlisteners.getExtensionMessageListeners(constructs, targets.responses)
}
	
	// Page Listeners in Background Script
function pageMessagesBack(constructs){
	let targets = targetter.prepareTargets(),
		extports = mlisteners.getExtensionConnectPorts(constructs, targets.extargets_c),
		[ehandlers, ematches] = getConnectPortsHandlers(extports),
		[extListeners, matches] = mlisteners.getExtensionMessageListeners(constructs, targets.extargets)
	return [extListeners.concat(ehandlers), Object.assign({}, matches, ematches)]
}

	// Extensions Messages Listeners
	// Page Listeners in Background Script
function extensionMessages(constructs){
	let targets = targetter.prepareTargets(),
		eports = mlisteners.getExtensionConnectPorts(constructs, targets.etargets_c),
		[ehandlers, ematches] = getConnectPortsHandlers(eports),
		aports = mlisteners.getExtensionConnectAssigner(constructs, targets.ctargets), // port = runtime.connect()
		[ahandlers, amatches] = getConnectPortsHandlers(aports),
		[extListeners, matches] = mlisteners.getExtensionMessageListeners(constructs, targets.etargets)
		
	return [extListeners.concat(ehandlers).concat(ahandlers), 
		Object.assign({}, matches, ematches, amatches)]
}

	//Get calls for sending messages from content scripts to back
function extensionSendMessagesCalls(constructs, pointers){
	let targets = targetter.prepareTargets(),
		eports = mlisteners.getExtensionConnectPorts(constructs, targets.etargets_c),
		aports = mlisteners.getExtensionConnectAssigner(constructs, targets.ctargets)		
		dtargets = [],
		prefs = targetter.prefs,
		psuffix = targetter.psuffix,
		ports = Object.assign({}, eports, aports)
		//Generate dynamic targets
	stargets = targets.sendMessage
	for(let port in ports){
		let ptargets = targetter.dynamicTargets(port +  psuffix, prefs)
		if(ptargets)
			stargets = stargets.concat(ptargets)
	}
	return mlisteners.getMatchedFunctions2(pointers, stargets)
}
	
	//handling dynamically injected codes
function handleDynamicCodes(codes){
	let results = []
		dccodes = []
	for(let c=0; c<codes.length; c++){
		let cconstructs = astvisit.parseAndVisit(codes[c].join("\n"))
		results.push(cconstructs)
		dccodes.push(codes[c].join("\n"))
	}
	return results
}
	//Handling dynamically injected files
function handleDynamicFiles(files, prefix){
	let results = [],
		dccodes = []
	for(let f=0; f<files.length; f++){
		let cscript = reader.gatherDynamicScripts(files[f], prefix),
			cconstructs = astvisit.parseAndVisit(cscript)
		results.push(cconstructs)
		dccodes.push(cscript)
	}
	return results
}

	// Handle Content Scripts
function handleContentScript(cconstructs){
	let pageHandlers = pageMessagesCScripts(cconstructs),
		portHandlers = getPortListeners(cconstructs),
		extHandlers = getExtensionListeners(cconstructs),
		postMessages = postMessageCalls(cconstructs),
		extcinjection = extensionContentInjection(cconstructs)

		return {
		"ports": portHandlers[1],
		"extension": extHandlers.concat(portHandlers[0].extension),
		"posts": postMessages,
		"page": pageHandlers,
		"injection": extcinjection
	}
}


	//Handle Background Script. External listeners
function handleBackgroundScript(bconstructs, external, corsPerm){
	if(bconstructs){
		let portHandlers = getPortListeners(bconstructs),
			[extHandlers, extMatches] = getExtensionListeners(bconstructs),
			pageHandlers = reqHandlers = resHandlers = [],
			pageMatches = reqMatches = resMatches = {}
		if(external)
			[pageHandlers, pageMatches] = pageMessagesBack(bconstructs)
		if(corsPerm){
			[reqHandlers, reqMatches] = getRequestsListeners(bconstructs)
			[resHandlers, resMatches] = getResponsesListeners(bconstructs)
		}
		return [{
			"ports": portHandlers[1],
			"extension": extHandlers.concat(portHandlers[0].extension),
			"external": pageHandlers.concat(portHandlers[0].external),
			"requests": reqHandlers,
			"responses": resHandlers
		}, {
			"ports": portHandlers[2],
			"extension": extMatches,
			"external": pageMatches,
			"requests": reqMatches,
			"responses": resMatches
		}]
	}
	return [{}, {}]
}


	//Dynamic code injection in background and UI pages
function handleDynanicContentScripts(constructs, prefix){
	let targets = targetter.prepareTargets(),
		[exres, matches] = mlisteners.getTabsExecuteScriptArguments(constructs, targets.tabs),
		results = {}


	if(exres){
		if("file" in exres)
			results.file = handleDynamicFiles(exres.file, prefix)
		if("code" in exres)
			results.code = handleDynamicCodes(exres.code, prefix)
	}
	return [results, exres]
}


//Gather constructs corresponding to handlers...and other functions that they call
function analyzeHandlers(handlers, constructs){
	//Parse handler
	let results = {},
		pointers = {},
		params = {},
		allcalls = {}
	for(let h=0; h<handlers.length; h++){
		let hcons = astvisit.initiateVisits(handlers[h].body)[0],
			lconstructs = [hcons],
			fpointers = {}
			//console.log(JSON.stringify(handlers[h].params))
			//Get all function pointers...
		for(let icall in hcons.icalls){
			if (icall in constructs.ifuncs){
				for(let ccall in constructs.ifuncs[icall]){
					fpointers[ccall] = ""
				}
			}
		}
		//
		for(let hicall in hcons.icalls){
			fpointers[hicall] = ""
		}
		for(let icall in fpointers){
			if (constructs.funcs && (icall in constructs.funcs)){
				for(let f=0; f<constructs.funcs[icall].length; f++){
					lconstructs.push(astvisit.initiateVisits(constructs.funcs[icall][f])[0])
				}
			}
		}
		results[h] = lconstructs
		pointers[h] = fpointers
		params[h] = astvisit.getFunctionParamaters(handlers[h])
	}
	return [results, pointers, params]
}



//Analyze handler constructs
/**
*** CORS: new XMLHttpRequest, Fetch, $.get, $.ajax, $.post
*** Storage: local.get, local.set, sync.set, sync.get
*** TStorage: localStorage.getItem, localStorage.setItem
*** Content Injection: getURL
*** Code: eval, aeval (aeval = eval)
*** Discovery: postMessage, addEventListener('message')
*** Transmission: port.postMessage, sendMessage
*** Receive and transmit: onMessage.addListener, postMessage
*** sendResponse in background script
*** Modifying CORS headers:
*** 	Look for Origin and Response
**/
function analyzeHandlerConstructs(handler){

}
	//Lookup Origin in Request
function analyzeBackgroundRequests(handlers, constructs){
	if(handlers){
		let [ares, pointers] = analyzeHandlers(handlers, constructs)
	}
}
	
	//If we have a request and response listers, then
	/**
	*** Analyse requests and responses listeners.
	*** If one is present, then resolve literals, identifers
	*** and expressions, seeking for "Origin", "ACAO", and "ACAC"
	**/
function analyzeCORSModifications(reqHandlers, respHandlers, constructs, matches){
	let originH = "Origin",
		ACAO = "Access-Control-Allow-Origin",
		ACAC = "Access-Control-Allow-Credentials",
		results = {}



	if((reqHandlers && reqHandlers.length > 0) ||
		(respHandlers && respHandlers.length > 0)){
		let [requh, requp] = analyzeHandlers(reqHandlers, constructs),
			[resph, respp] = analyzeHandlers(respHandlers, constructs)
		let reqOrigin = resolveToLiteral(requh, constructs, "Literal", originH),
			respACAO = resolveToLiteral(resph, constructs, "Literal", ACAO),
			respACAC = resolveToLiteral(resph, constructs, "Literal", ACAC)
		//console.log("Origin ", reqOrigin, "ACAO ", respACAO, "ACAC ", respACAC)
		
		results["local"] = {
			"Origin": reqOrigin,
			"ACAO": respACAO,
			"ACAC": respACAC
		}
	}

	//if (("requests" in matches) || ("responses" in matches)) {
		results["global"] = {
			"Origin": (originH in constructs.l || (originH.toLowerCase() in constructs.l)),
			"ACAO": ((ACAO in constructs.l) || (ACAO.toLowerCase() in constructs.l)),
			"ACAC": ((ACAC in constructs.l) || (ACAC.toLowerCase() in constructs.l))
		};
	//}
	return results
}
	
	//Resolve Request and Response Listeners to Literals
function resolveToLiteral(handlers, constructs, type, value){
	let results = {
		l: {},
		i: {},
		me: {},
		le: {},
		be: {}
	}
	for(let hkey in handlers){
		for(let h=0; h<handlers[hkey].length; h++){
			let handler = handlers[hkey][h]
			for(let key in results){
				if(handler[key]){
					if (results[key] instanceof Array)
						results[key] = results[key].concat(handler[key])
					else 
						results[key] = Object.assign({}, results[key], handler[key])
				}
			}
		}
	}
		
		//Get literals First
	let assigns = Object.keys(results.l)	
		//Get assignments to identifiers and member expressions	
	for(let id in results.i){
		let litass = asthandler.getAssignmentsOfType(id, constructs, type)
		assigns = assigns.concat(litass)
	}

	for(let me in results.me){
		let meass = asthandler.getAssignmentsOfType(me, constructs, type)
		assigns = assigns.concat(meass)
	}


	//Resolve Logical and Binary Expressions to Literal
	for(let le in results.le){
		if(le == "&&" || le == "||"){
			for(let l=0; l<results.le[le].length; l++){
				let lass = asthandler.resolveExpressionOfType(results.le[le][l], constructs, type)
				assigns = assigns.concat(lass)
			}
		}
	}
		//Binary Expressions
	for (let be in results.be){
		if(be == "+"){
			for(let b=0; b<results.be[be].length; b++){
				let bass = asthandler.resolveExpressionOfType(results.be[be][b], constructs, type)
				assigns = assigns.concat(bass)
			}
		}
	}
		//Check whether the literals resolve to the specified literal
	let foundLocal = false
	for(let a=0; a<assigns.length; a++){
		if(asthandler.resolveExpression(assigns[a], value, constructs))
			return true
	}
		//Check the whole construct literals

	
	return false
}

	//Lookup the use of eval, setTimeout, setInterval on string arguments
function lookupEval(constructs){
	return {
		i: ("eval" in constructs.i), // Aliasing and callback
		l: ("eval" in constructs.l), // window["eval"]
		c: ("eval" in constructs.icalls)
	}
}

	//Get setdata, getdata, or ajax
function getMatches(pointers, handlers, setOrGetOrAjax){
	let targets = targetter.prepareTargets(),
		itargets = targetter.itargets,
		matched = mlisteners.getMatchedFunctions2(pointers, targets[setOrGetOrAjax])
	//Matched Identifiers and literals
		matchesLI = mlisteners.getMatchedIdentifiersLiterals(handlers, itargets[setOrGetOrAjax])

	return Object.assign({}, {"calls": matched}, matchesLI)
}
	//Get ajax and eval in global constructs
function getGlobalMatches(constructs, evalsOrAjax){
	let targets = targetter.prepareTargets(),
		itargets = targetter.itargets,
		matched = mlisteners.getMatchedFunctions(constructs, targets[evalsOrAjax])
	//Matched Identifiers and literals
		matchesLI = mlisteners.getMatchedIdentifiersLiteralsGlobal(constructs, itargets[evalsOrAjax])
	return Object.assign({}, {"calls": matched}, matchesLI)
}

	//Get ajax and eval in global constructs
function getGlobalMatchesI(constructs, evalsOrAjax){
	let targets = targetter.prepareTargets(),
		itargets = targetter.itargets,
		matched = mlisteners.getMatchedFunctions(constructs, targets[evalsOrAjax])
	//Matched Identifiers and literals
		matchesLI = mlisteners.getMatchedIdentifiersLiteralsGlobalI(constructs, itargets[evalsOrAjax])
	return Object.assign({}, {"calls": matched}, matchesLI)
}



module.exports  = {
	pageMessagesBack: pageMessagesBack,
	pageMessagesCScripts: pageMessagesCScripts,
	extensionMessages: extensionMessages,
	extensionSendMessagesCalls: extensionSendMessagesCalls,
	getRequestsListeners: getRequestsListeners,
	getResponsesListeners: getResponsesListeners,
	handleContentScript: handleContentScript,
	handleBackgroundScript: handleBackgroundScript,
	handleDynanicContentScripts: handleDynanicContentScripts,
	analyzeHandlers: analyzeHandlers,
	analyzeCORSModifications: analyzeCORSModifications,
	lookupEval: lookupEval,
	getMatches: getMatches,
	getGlobalMatches: getGlobalMatches,
	getGlobalMatchesI: getGlobalMatchesI
}