/*
  
A tool for analyzing the message passing APIs 
between browser extensions and web applications
   
   Copyright (C) 2017 - 2019 INRIA
                                                                     
    Bug descriptions, user reports, comments or suggestions are   
    welcome. Send them to doliere.some@inria.fr or some.frncs@gmail.com
                                   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or 
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,   
   but WITHOUT ANY WARRANTY; without even the implied warranty of    
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     
   GNU General Public License for more details.                      
                                                                     
   You should have received a copy of the GNU General Public         
   License along with this program; if not, write to the Free        
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,   
   MA 02111-1307, USA.  

*/
// =======================================================================================
//    results.js
//    ------------------------------------------------------------------------------------           
//    Author      :  Dolière Francis Somé                                    
//    Creation    :  Monday January 21 11:04:40 2019                          
//    Last change :  Monday January 21 11:04:40 2019               
//    Copyright   :  2017-2019 Dolière Francis Somé                          
// =======================================================================================


const fs = require('fs'),
	reader = require('./reader')



let dir = process.argv[2],
	browser = process.argv[3],
	unzip = process.argv[4],
	out = process.argv[5]



var [cfiles, bfiles, manifests] = readFiles(dir, unzip, browser);

extensionsToAnalyze(bfiles, cfiles, manifests, out);



function extensionsToAnalyze(bfiles, cfiles, manifests){
	var /*reskeys = ["respond", "evals", "setData", "getData", "ajax", 
		"management", "history", "bookmarks", "browsingData", 
		"cookies", "downloads", "tabs", "topSites"
	];*/

	reskeys = ["evals", "setData", "getData", "ajax", "management", "history", "bookmarks", "browsingData", "cookies", "downloads", "topSites"];
	//reskeys = ["setData", "getData"];
	//reskeys = ["respond"]

	let bres = {}, cres = {cs: {}, dcs: {}, pages: {}};
	for(let r=0; r<reskeys.length; r++){
		key = reskeys[r];
		bres[key] = analyzeBackgroundPage(bfiles, browser, key, manifests)
		cres.cs[key] = analyzeContentScripts(cfiles, browser, key, "cs", manifests)
		cres.dcs[key] = analyzeContentScripts(cfiles, browser, key, "dcs", manifests)
		cres.pages[key] = analyzeContentScripts(cfiles, browser, key, "pages", manifests)
	}

	var fbres = gatherResultsBackground(bres),
		fcres = gatherResultsContentScripts(cres);
	//console.log(JSON.stringify(fbres, null, 4));
	//console.log(JSON.stringify(fres.cs, null, 4));


	var fresults = {}

	for(let ext in fcres){
		fresults[ext] = {
			com_via_cs: fcres[ext]
		}
	}

	for(let ext in fbres){
		if(!(ext in fresults))
			fresults[ext] = {}
		fresults[ext].com_via_bs = fbres[ext];
	}
	//console.log(JSON.stringify(fresults, null, 4));
	console.log("NB EXTENSIONS ", Object.keys(fresults).length);
	fs.writeFile(out, JSON.stringify(fresults), function(){})
	//return fresults;
}


function contentInjection(cfiles, key){
	var cres = {cs: {}, dcs: {}}
	cres.cs[key] = analyzeContentScripts(cfiles, browser, key, "cs", manifests)
	cres.dcs[key] = analyzeContentScripts(cfiles, browser, key, "dcs", manifests)

	fcres = gatherResultsContentScripts(cres);
	console.log("NB EXTENSIONS ", Object.keys(fcres).length);
	fs.writeFile(browser + "_" + key + ".json", JSON.stringify(fcres), function(){})
	//return fresults;
}
function contentInjections(cfiles){
	var keys = ["content_injection", "content_injection_", "getURL"],
		fres = {};
	for(let k=0; k<keys.length; k++){
		var cres = {cs: {}, dcs: {}},
			key = keys[k];
		cres.cs[key] = analyzeContentScripts(cfiles, browser, key, "cs", manifests)
		cres.dcs[key] = analyzeContentScripts(cfiles, browser, key, "dcs", manifests)

		fres[key] = gatherResultsContentScripts(cres);
	}
	//console.log("NB EXTENSIONS ", Object.keys(f).length);
	fs.writeFile(browser + "_cis.json", JSON.stringify(fres), function(){})
	//return fresults;
}




function checkPermission(manifests, key, ext){
	var permKeys = { 
		"setData": "storage",
		"getData": "storage",
		"management": "management",
		"history": "history", 
		"bookmarks": "bookmarks", 
		"browsingData":"browsingData",
		"cookies": "cookies",
		"downloads": "downloads",
		"topSites": "topSites"
	},
	permissions = manifests[ext];

	return ((!(key in permKeys)) || (permissions.indexOf(permKeys[key]) >= 0))
}


function gatherResultsBackground(bres){
	let fbres = {}
	for(let key in bres){
		for(let ext in bres[key]){
			if(!(ext in fbres))
				fbres[ext] = {}
			for(let bp in bres[key][ext]){
				if(!(bp in fbres[ext]))
					fbres[ext][bp] = {}
				fbres[ext][bp][key] = bres[key][ext][bp]
			}
		}
	}
	return fbres
}


function gatherResultsBackground(bres){
	let fbres = {}
	for(let key in bres){
		for(let ext in bres[key]){
			if(!(ext in fbres))
				fbres[ext] = {}
			for(let bp in bres[key][ext]){
				if(!(bp in fbres[ext]))
					fbres[ext][bp] = {}
				fbres[ext][bp][key] = bres[key][ext][bp]
			}
		}
	}
	return fbres
}

function gatherResultsContentScripts(cres){
	let fcres = {cs: {}, dcs: {}, pages: {}};
	for(let cdp in cres){
		if(cdp != "pages"){
			for(let key in cres[cdp]){
				for(let ext in cres[cdp][key]){
					if(cdp == "cs"){
						if(!(ext in fcres[cdp]))
							fcres[cdp][ext] = {}
						for(let epmsg in cres[cdp][key][ext]){
							for(let group in cres[cdp][key][ext][epmsg]){
								//console.log("GROUP ", group);
								if(!(group in fcres[cdp][ext]))
									fcres[cdp][ext][group] = {}
								if(!(epmsg in fcres[cdp][ext][group]))
									fcres[cdp][ext][group][epmsg] = {}
								fcres[cdp][ext][group][epmsg][key] = cres[cdp][key][ext][epmsg][group]
							}
						}
					}else if(cdp == "dcs"){
						for(let filecode in cres[cdp][key][ext]){
							for(let bp in cres[cdp][key][ext][filecode]){
								console.log("BP ", bp);
								for(let epmsg in cres[cdp][key][ext][filecode][bp]){
									for(let group in cres[cdp][key][ext][filecode][bp][epmsg]){
										if(!(ext in fcres["cs"]))
											fcres["cs"][ext] = {}
										if(!(group in fcres["cs"][ext]))
											fcres["cs"][ext][group] = {}
										if(!(filecode in fcres["cs"][ext][group]))
											fcres["cs"][ext][group][filecode] = {}
										fcres["cs"][ext][group][filecode][key] = cres[cdp][key][ext][filecode][bp][epmsg][group]
										//if(!(epmsg in fcres["cs"][ext][group][filecode]))
										//fcres["cs"][ext][group][filecode][epmsg] = {}
										//fcres["cs"][ext][group][filecode][epmsg][key] = cres[cdp][key][ext][filecode][bp][epmsg][group]
									}
								}
							}
						}
						/*
						if(!(epmsg in fcres[cdp][ext]))
							fcres[cdp][ext][epmsg] = {}
						fcres[cdp][ext][epmsg][key] = cres[cdp][key][ext]*/
					}
				}
			}
		}
	}

	var lpages = gatherResultsBackground(cres.pages)
	for(let ext in lpages){
		if(!(ext in fcres.cs))
			fcres.cs[ext] = {}
		fcres.cs[ext].to_back = lpages[ext]
	}
	return fcres.cs
}










	// Read files
function readFiles(dir, mdir, browser){
	var files = fs.readdirSync(dir),
		jfiles = {},
		cfiles = {},
		bfiles = {},
		manifests = {};

	console.log("Files ", files.length, " Browser ", browser);

	for(let f=0; f<files.length; f++){
		console.log("FILE ", f, "/", files.length)
		let fpath = dir + files[f];
		//
		try{
			let data = JSON.parse(fs.readFileSync(fpath));
			jfiles[files[f].slice(0, -5)] = data;
			cfiles[files[f].slice(0, -5)] = data[0];
			bfiles[files[f].slice(0, -5)] = data[1]
			manifests[files[f].slice(0, -5)] = reader.parseManifest(mdir + files[f].slice(0, -5)).permissions || []
			//console.log(manifests)
			//if(Object.keys(data[1]).length > 0)
			//console.log(data[1])
		}catch(e){}
	}
	//console.log(jfiles);
	return [cfiles, bfiles, manifests];
}


function analyzeBackgroundPage(extensions, browser, key, manifests){
	let results = {
		calls: {},
		literals: {},
		identifiers: {}
	},
	res = {}
	//console.log("Extensions", extensions)
	for(let ext in extensions){
		//if("pmsg" in extensions[ext]){
		//onsole.log("EXT", extensions[ext])
		if(key in extensions[ext] && checkPermission(manifests, key, ext)){
			for(let page in extensions[ext][key]){ // back, or ui or page
				if(key == "respond"){
					if(extensions[ext][key][page].length){
						console.log("EXT", ext, "===>", extensions[ext][key][page])
						if(!(ext in res))
							res[ext] = {}
						if(!(page in res[ext]))
							res[ext][page] = []
						res[ext][page] = res[ext][page].concat(extensions[ext][key][page])
						
					}
				}else{
					if(Object.keys(extensions[ext][key][page]).length){
						for(let cli in results){
							if(extensions[ext][key][page][cli] && 
								Object.keys(extensions[ext][key][page][cli]).length){
								console.log("EXT", ext, "==>", extensions[ext][key][page][cli])
							
								if(!(ext in res))
									res[ext] = {}
								if(!(page in res[ext]))
									res[ext][page] = {}
								res[ext][page] = Object.assign({}, extensions[ext][key][page][cli], res[ext][page])

							}
						}
					}
				}
			}
		}
		//}
	}
	return res
}



function analyzeContentScripts(jfiles, browser, key, cdp, manifests){
	//jfiles = jfiles[0];
	if(cdp){
		let res = {}
		switch(cdp){
			case "cs":
				console.log("=========== Content Scripts ============")
				res = contentScripts(jfiles, browser, key, manifests)
				break;
			case "dcs":
				console.log("=========== Dynamic Content Scripts ==========")
				res = dynamicContentScripts(jfiles, browser, key, manifests)
				break;
			case "pages":
				console.log("=========== Background and UI Pages ==========")
				res = extensionsPages(jfiles, browser, key, manifests)
				break;
			default:
				break;
		}
		//console.log(res)
		return res
	}
}

	//Content Scripts
function contentScripts(extensions, browser, key, manifests){
	let results = {}
	for (let ext in extensions){
		if("cs" in extensions[ext] && checkPermission(manifests, key, ext)){
			let lres = handleCSData(extensions[ext].cs, ext, browser, key)
			if(Object.keys(lres).length)
				results[ext] = lres
		}
	}
	return results
}


	//Dynamic Content Scripts
function dynamicContentScripts(extensions, browser, key){
	let results = {}
	for (let ext in extensions){
		if("dcs" in extensions[ext] && checkPermission(manifests, key, ext)){
			for(let page in extensions[ext].dcs){
					//Page Messages
				if("file" in extensions[ext].dcs[page]){
					let fres = handleCSData(extensions[ext].dcs[page].file, ext, browser, key)
					if(Object.keys(fres).length){
						if(!(ext in results))
							results[ext] = {}
						if(!("file" in results[ext]))
							results[ext].file = {}
						results[ext].file[page] = fres 
					}
				}

				if("code" in extensions[ext].dcs[page]){
					let cres = handleCSData(extensions[ext].dcs[page].code, ext, browser, key)
					if(Object.keys(cres).length){
						if(!(ext in results))
							results[ext] = {}
						if(!("code" in results[ext]))
							results[ext].code = {}
						results[ext].code[page] = cres 
					}
				}
			}
		}
	}
	return results
}

	//Extensions Pages Data
function extensionsPages(extensions, browser, key, manifests){
	let res = {}
	for(let ext in extensions){
		if("pages" in extensions[ext] &&  checkPermission(manifests, key, ext)){
			for(let pg in extensions[ext].pages){
				let lres = pageMessages(ext, extensions[ext].pages[pg], browser, key)
				if(Object.keys(lres).length){
					if(!(ext in res))
						res[ext] = {}
					res[ext][pg] = lres
				}
			}
		}
	}
	return res
}


	
	//Print (dynamic) content script data
function handleCSData(data, ext, browser, key){
	//console.log("DATA", data, "EXT ", ext, "Browser", browser, "Key", key)
	let pres = {},
		eres = {}
	for(let c in data){
			//Page Messages
		if("pmsg" in data[c]){
			let lpres = pageMessages(ext, data[c].pmsg, browser, key)
			if(Object.keys(lpres).length)
				pres[c] = lpres
		}
			//Extension messages
		if("emsg" in data[c]){
			let leres = pageMessages(ext, data[c].emsg, browser, key)
			if(Object.keys(leres).length)
				eres[c] = leres
		}
	}
	let fres = {}
	if(Object.keys(pres).length)
		fres.pmsg = pres
	if(Object.keys(eres).length)
		fres.emsg = eres
	return fres
}
	//Page Messages
function pageMessages(ext, data, browser, key){
	let results = {
		calls: {},
		literals: {},
		identifiers: {}
	},
	res  = {}
	//console.log("DATA ", data)
	if(key in data && Object.keys(data[key]).length > 0){
		if(key == "respond"){
			//console.log("EXT", ext, "===>", Object.keys(data[key]))
			return data[key]
		}else if (key == "content_injection" || key == "content_injection_"){
			//console.log("EXT", ext, "===>", Object.keys(data[key]))
			return data[key]
		}/*else if(key == "postMessages"){
			console.log("EXT", ext, "===>", Object.keys(data[key]))		
		}*/else{
			let lres = getRes(data, key, results)
			if(key == "setData")
				lres = Object.assign({}, lres, getRes(data, "getData", results, ext))
			else if(key == "getData")
				lres = Object.assign({}, lres, getRes(data, "setData", results, ext))
			return lres
		}
	}
	return res

}


function getRes(data, key, results, ext){
	let lres = {}
	for(let cli in results){
		if(data[key][cli] && 
			Object.keys(data[key][cli]).length){
			if(key == "evals"){
				if(cli == "calls" && "eval" in data[key][cli]){
					//console.log("EXT", ext, "==>", Object.keys(data[key][cli]))
					lres = Object.assign({}, lres, data[key][cli])
				}
			}else{
				//console.log("EXT", ext, "==>", Object.keys(data[key][cli]))
				lres = Object.assign({}, lres, data[key][cli])
			}
		}
	}
	return lres
}
