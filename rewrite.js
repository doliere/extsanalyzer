/*
  
A tool for analyzing the message passing APIs 
between browser extensions and web applications
   
   Copyright (C) 2017 - 2019 INRIA
                                                                     
    Bug descriptions, user reports, comments or suggestions are   
    welcome. Send them to doliere.some@inria.fr or some.frncs@gmail.com
                                   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or 
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,   
   but WITHOUT ANY WARRANTY; without even the implied warranty of    
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     
   GNU General Public License for more details.                      
                                                                     
   You should have received a copy of the GNU General Public         
   License along with this program; if not, write to the Free        
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,   
   MA 02111-1307, USA.  

*/
// =======================================================================================
//    rewrite.js
//    ------------------------------------------------------------------------------------           
//    Author      :  Dolière Francis Somé                                    
//    Creation    :  Monday January 21 11:04:40 2019                          
//    Last change :  Monday January 21 11:04:40 2019               
//    Copyright   :  2017-2019 Dolière Francis Somé                          
// =======================================================================================


const jsdom = require("jsdom");
const { JSDOM } = jsdom


function reWriteHTML(rbody){
  var results = []
  var inlineScripts = []

  var dom = new JSDOM(rbody)
  var document = dom.window.document

    //Scripts
  var scripts = document.getElementsByTagName("script");
  for(var s=0; s<scripts.length; s++){
    var script = scripts[s]
    if(!script.type || script.type === "text/javascript" || (script.language && script.language.indexOf('javascript') >=0)){
      var srcc = script.getAttribute('src') // Get the src value here
      if(srcc){
     	  results.push(srcc)
      }else{
        inlineScripts.push(script.innerHTML)
      }
    }
  }

    //Iframes
  let iresults = []
  var iframes = document.getElementsByTagName("iframe");
  for(var i=0; i<iframes.length; i++){
    var iframe = iframes[i]
    var srcc = iframe.getAttribute('src') // Get the src value here
    if(srcc){
      iresults.push(srcc)
    }
  }

    //Links
  let lresults = []
  var links = document.getElementsByTagName("a");
  for(var l=0; l<links.length; l++){
    var link = links[l]
    var href = link.getAttribute('href') // Get the src value here
    if(href){
      lresults.push(href)
    }
  }

  return [results, inlineScripts.join("\n\n"), iresults, lresults]
}

module.exports = {
	parse: reWriteHTML
}
