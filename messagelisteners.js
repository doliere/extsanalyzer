/*
  
A tool for analyzing the message passing APIs 
between browser extensions and web applications
   
   Copyright (C) 2017 - 2019 INRIA
                                                                     
    Bug descriptions, user reports, comments or suggestions are   
    welcome. Send them to doliere.some@inria.fr or some.frncs@gmail.com
                                   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or 
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,   
   but WITHOUT ANY WARRANTY; without even the implied warranty of    
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     
   GNU General Public License for more details.                      
                                                                     
   You should have received a copy of the GNU General Public         
   License along with this program; if not, write to the Free        
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,   
   MA 02111-1307, USA.  

*/
// =======================================================================================
//    messagelisteners.js
//    ------------------------------------------------------------------------------------           
//    Author      :  Dolière Francis Somé                                    
//    Creation    :  Monday January 21 11:04:40 2019                          
//    Last change :  Monday January 21 11:04:40 2019               
//    Copyright   :  2017-2019 Dolière Francis Somé                          
// =======================================================================================


const asthandler = require('./asthandler'),
	recast = require('recast')

	/**
	*** Generic function for extracting DOM and Extension message listeners.
	*** Get handlers of addEventListener('message', f), 
	**/ 
function getWindowMessageListeners(constructs, targets){
	let results = getMatchedFunctions(constructs, targets)
	//console.log("Matched Functions", results, targets)
	let funcs = [],
		matches = {}
	for(let key in results ){
		for(let tkey in constructs.icalls[key]){
			for(let k=0; k<constructs.icalls[key][tkey].length; k++){
				if(!constructs.icalls[key][tkey][k][2]){ // Not bind
					//Check that first argument matches message
					let largs = constructs.icalls[key][tkey][k]
					if(largs[1].length >= 2 && !largs[2]){ // Not bound function
						largs = largs[1]
						//Resolve first argument to message
						let marg = asthandler.resolveExpression(largs[0], "message", constructs)
						if(marg){ // We have a message listener
							//Resolve handler to a function or a set of functions.
							let lfuncs = asthandler.resolveFunction(largs[1], constructs)
							if(lfuncs)
								funcs = funcs.concat(lfuncs)
							matches[key] = ""
						}
						//console.log("MARG ", marg)
					}
				}
			}
		}
	}
	//return [funcs, results]
	return [funcs, matches]
}


	/**
	*** Generic function for extracting DOM and Extension message listeners.
	*** window.onmessage = function(){}
	**/ 
function getWindowMessageListenersAssignment(constructs, targets){
	let results = getMatchedAssignments(constructs, targets),
		funcs = [],
		ftypes = {
			"FunctionExpression": "", 
			"FunctionDeclaration": "",
			"ArrowFunctionExpression": ""
		}
	//Get the function expression and declarations corresponging
	for(let key in results){
		//console.log("OMSG ", key)
		for(let ftype in ftypes){
			let res = asthandler.getAssignmentsOfType(key, constructs, ftype)
			//console.log("RES ", res)
			if(res)
				funcs = funcs.concat(res)
		}
	}
	return [funcs, results]
}



	/**
	*** Generic function Extensions Listeners.
	*** Get handlers of runtime.onMessage.addListener
	**/ 
function getExtensionMessageListeners(constructs, targets){
	let results = getMatchedFunctions(constructs, targets)
	let funcs = []
	for(let key in results ){

		for(let tkey in constructs.icalls[key]){
			for(let k=0; k<constructs.icalls[key][tkey].length; k++){
				if(!constructs.icalls[key][tkey][k][2]){ // Not bind
					//Check that first argument matches message
					let largs = constructs.icalls[key][tkey][k]
					if(largs[1].length >= 1){
							//Resolve handler to a function or a set of functions.
						let lfuncs = asthandler.resolveFunction(largs[1][0], constructs)
						funcs = funcs.concat(lfuncs)
					}
				}
			}
		}
	}
	return [funcs, results]
}

/**
	*** Generic function for extracting DOM and Extension message listeners.
	*** Get handlers of addEventListener('message', f), 
	**/ 
function getMatchedFunctionsNbArguments(constructs, targets){
	let results = getMatchedFunctions(constructs, targets),
		fresults = {}
	//console.log("Matched Functions", results)
	let funcs = []
	for(let key in results ){
		for(let tkey in constructs.icalls[key]){
			for(let k=0; k<constructs.icalls[key][tkey].length; k++){
				if(!constructs.icalls[key][tkey][k][2]){ // Not bind
					let largs = constructs.icalls[key][tkey][k]
					if(! (key in fresults))
						fresults[key] = {}
					fresults[key] = largs[1].length
				}
			}
		}
	}
	return fresults
}



	/**
	*** Generic function Extensions Listeners.
	*** Get handlers of runtime.onConnect.addListener(function(port))
	**/ 
function getExtensionConnectPorts(constructs, targets){

	let [funcs, mfuncs] = getExtensionMessageListeners(constructs, targets),
		results = {},
		hresults = {} // Handlers
	if(funcs){
		for(let f=0; f<funcs.length; f++){
			//Get arguments, and the corresponding port...
			let params = funcs[f].params
			if(params && params.length >=0){
				let port = params[0]
				if(port && (port.type == "Identifier")){ // Should be identifier
					let pcast = recast.prettyPrint(port).code
					results[pcast] = ""
					if(!(pcast in hresults))
						hresults[pcast] = []
					hresults[pcast].push(funcs[f].body)
				}
			}

		}
	}
	return hresults //[results, hresults]
}


/**
	*** Generic function Extensions Listeners.
	*** Get handlers of runtime.onMessage.addListener
	**/ 
function getExtensionConnectAssigner(constructs, targets){
	let results = getMatchedFunctions(constructs, targets)
	let funcs = {}
	if(constructs.acalls){
		for(let key in results ){
			for(let lcast in constructs.acalls){
				if(key in constructs.acalls[lcast])
					funcs[lcast] = ""
			}
		}
	}

	return funcs
}


	/**
	*** Generic function Extensions Listeners.
	*** Get handlers of runtime.onMessage.addListener
	**/ 
function getTabsExecuteScriptArguments(constructs, targets){
	let results = getMatchedFunctions(constructs, targets)
	let executes = {}
	for(let key in results ){
		for(let tkey in constructs.icalls[key]){
			for(let k=0; k<constructs.icalls[key][tkey].length; k++){
				if(!constructs.icalls[key][tkey][k][2]){ // Not bind
					//Check that first argument matches message
					let largs = constructs.icalls[key][tkey][k][1],
						fargs;
					if(largs.length == 1){
						fargs = largs[0]
					}else if(largs.length >= 2){
						fargs = largs[1]
					}
					let obj = asthandler.resolveExpressionOfType(fargs, constructs, "ObjectExpression")
					obj = extractObjectExpressionProperties(obj, constructs)

					//Extract files, codes...Later on, we extract more...
					if(obj){
						for(let okey in obj){
							if(! (okey in executes))
								executes[okey] = []
							executes[okey] = executes[okey].concat(obj[okey])
						}
					}
				}
			}
		}
	}
	return [executes, results]
}
	
	//
function extractObjectExpressionProperties(expressions, constructs){
	let results = {}
	for(let e=0; e<expressions.length; e++){
		let expression = expressions[e]
		if(expression.type == "ObjectExpression"){
			let properties = expression.properties
			for(let p=0; p<properties.length; p++){
				let prop = properties[p],
					pkey = prop.key.type == "Literal" ? prop.key.value: prop.key.name,
					pvalue = prop.value
					if(pkey == "file" || pkey == "code"){
						//Resolve pvalue as an expression
						//console.log("P Value ", pvalue)
						let values = asthandler.resolveExpressionOfType(pvalue, constructs, "Literal")
						//console.log("VALUES ", values)
						if(values){
							for(let v=0; v<values.length; v++){
								values[v] = values[v].value
							}
						}
						if(!(pkey in results))
							results[pkey] = []
						results[pkey].push(values)
					}

			}
		}
	}
	return results
}

	//Get matched functions
function getMatchedFunctions(constructs, targets){
	let results = {}
	for(let t=0; t<targets.length; t++){
		let target = targets[t],
			pref = target.pref,
			tname = target.tname,
			nbargs = target.nbargs,
			checkPref = target.checkPref
		//console.log("Target ", target)
		if(constructs.icalls){
			for(let key in constructs.icalls){
				if (key == tname || 
					((checkPref || (tname.startsWith(".") || tname.startsWith("[")))
					 && key.endsWith(tname))){
					results[key] = ""
				}
			}
		}
	}
	return results
}



	//Get matched functions
function getMatchedFunctions2(constructs, targets){
	let results = {}
	for(let c in constructs){
		let res = getMatchedFunctions({"icalls": constructs[c]}, targets)
		results = Object.assign({}, res, results)
	}
	return results
}

	//Get matched functions
function getMatchedIdentifiersLiterals(constructs, targets){
	let results = {},
		literals = {},
		identifiers = {}
	for(let cst in constructs){
		//console.log(constructs[c])
		for(let c=0; c<constructs[cst].length; c++){
			identifiers = Object.assign({}, identifiers, constructs[cst][c].i)
			literals = Object.assign({}, literals, constructs[cst][c].l)
		}
	}

	results.literals = {}
	results.identifiers = {}

	for(let t=0; t<targets.length; t++){
		let target = targets[t]
		if(target in literals)
			results.literals[target] = true
		if(target in identifiers)
			results.identifiers[target] = true
	}
	return results
}


	//Get matched functions
function getMatchedIdentifiersLiteralsGlobal(constructs, targets){
	let results = {},
		literals = constructs.l,
		identifiers = constructs.i

	results.literals = {}
	results.identifiers = {}

	for(let t=0; t<targets.length; t++){
		let target = targets[t]
		if(target in literals)
			results.literals[target] = true
		if(target in identifiers)
			results.identifiers[target] = true
	}
	return results
}

	//Get matched identifiers and literals, case insensitive
function getMatchedIdentifiersLiteralsGlobalI(constructs, targets){
	let results = {},
		literals = constructs.l,
		identifiers = constructs.i

	results.literals = {}
	results.identifiers = {}

	for(let t=0; t<targets.length; t++){
		let target = (targets[t] + "").toLowerCase()
		for(var literal in literals){
			var lit = (literal + "").toLowerCase()
			if(target == lit){
				results.literals[target] = true
				break;
			}
		}
		for(var identifier in identifiers){
			var id = (identifier + "").toLowerCase()
			if(target == id){
				results.identifiers[target] = true
				break;
			}
		}
	}
	return results
}


	//Get matched functions
function getMatchedAssignments(constructs, targets){
	let results = {},
		keys = {"aliases": "", "aindex": ""}
	for(let akey in keys){
		if(constructs[akey]){
			for(let key in constructs[akey]){
				for(let t=0; t<targets.length; t++){
					let target = targets[t],
						pref = target.pref,
						tname = target.tname,
						nbargs = target.nbargs,
						checkPref = target.checkPref
					if (key == tname || 
						((checkPref || (tname.startsWith(".") || tname.startsWith("[")))
						 && key.endsWith(tname))){
						results[key] = ""
					}
				}
			}
		}
	}
	return results
}

module.exports = {
	getWindowMessageListeners: getWindowMessageListeners,
	getWindowMessageListenersAssignment: getWindowMessageListenersAssignment,
	getExtensionMessageListeners: getExtensionMessageListeners,
	getExtensionConnectAssigner: getExtensionConnectAssigner,
	getExtensionConnectPorts: getExtensionConnectPorts,
	getTabsExecuteScriptArguments: getTabsExecuteScriptArguments,
	getMatchedFunctionsNbArguments: getMatchedFunctionsNbArguments,
	getMatchedFunctions2: getMatchedFunctions2,
	getMatchedFunctions: getMatchedFunctions,
	getMatchedIdentifiersLiterals: getMatchedIdentifiersLiterals,
	getMatchedIdentifiersLiteralsGlobal: getMatchedIdentifiersLiteralsGlobal,
	getMatchedIdentifiersLiteralsGlobalI: getMatchedIdentifiersLiteralsGlobalI
}