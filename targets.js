/*
  
A tool for analyzing the message passing APIs 
between browser extensions and web applications
   
   Copyright (C) 2017 - 2019 INRIA
                                                                     
    Bug descriptions, user reports, comments or suggestions are   
    welcome. Send them to doliere.some@inria.fr or some.frncs@gmail.com
                                   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or 
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,   
   but WITHOUT ANY WARRANTY; without even the implied warranty of    
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     
   GNU General Public License for more details.                      
                                                                     
   You should have received a copy of the GNU General Public         
   License along with this program; if not, write to the Free        
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,   
   MA 02111-1307, USA.  

*/
// =======================================================================================
//    targets.js
//    ------------------------------------------------------------------------------------           
//    Author      :  Dolière Francis Somé                                    
//    Creation    :  Monday January 21 11:04:40 2019                          
//    Last change :  Monday January 21 11:04:40 2019               
//    Copyright   :  2017-2019 Dolière Francis Somé                          
// =======================================================================================


function generateTargets(fname, pref, checkPref, nbargs){
	let fnames = fname.split('.'),
		ffnames = fnames.slice(1),
		fprefix = fnames[0]
	results = {}, fresults = {}
	let pnumbers = getBinaries(fnames.length),
		fnumbers = getBinaries(fnames.length - 1)
    if(fnames.length == 1)
       fnumbers = ["0"]
	for(let p=0; p<pnumbers.length; p++){
		let ires = ""
		for(let pf=0; pf<pnumbers[p].length; pf++){
			let pfi = pnumbers[p][pf]
			if(pfi == "0")
				ires += '["' + fnames[pf] + '"]'
			else
				ires += "." + fnames[pf]
		}

		//Prepend Prefix
		if(!checkPref){
			for(let pr = 0; pr<pref.length; pr++){
				results[pref[pr] + ires] = ""
			}
		}else{
			//if(fnames.length > 2)
				results[ires] = ""
		}
	}

    //if(!fnumbers)
    
		//First
	if(fnumbers.length > 1){
	    for(let p=0; p<fnumbers.length; p++){
	        let ires = ""
	        for(let pf=0; pf<fnumbers[p].length; pf++){
	            let pfi = fnumbers[p][pf]
	            if(pfi == "0")
	                ires += '["' + ffnames[pf] + '"]'
	            else
	                ires += "." + ffnames[pf]
	        }

	        if(fnames.length >= 1){
	            if(ires && fprefix){

	                if(!checkPref){
	                    fresults[fprefix + ires] = ""
	                }else{
	                    //if(fnames.length > 2)
	                        fresults[ires] = ""
	                }
	            }
	        }
	    }
	}else{
		fresults[fprefix] = ""
	}

	// Take other attributes into account
	results = Object.assign({}, results, fresults)
	//return results
	let targets = []
	for(let key in results){
		let res = {
			"tname": key, 
			"checkPref": checkPref, 
			"nbargs": nbargs
		};
		targets.push(res)
	}
	return targets
}
	//Get the numbers
function getBinaries(nb){
	let rnb = Math.pow(2, nb),
		pre = new Array(rnb+1).fill(0).join(''),
		results = []
	for(let i=0; i<rnb; i++){
		let nres = i.toString(2)
			nres = pre.slice(0, nb-nres.length) + nres
		results.push(nres)
	}
	return results
}

let checkPref = false,
	prefs = ["this", "window", "self", "top"],
	esuffix = ".onMessage.addListener",
	psuffix = ".postMessage",
	asuffixes = [".href", ".src", ".data"],
	srcsuffix = [".src"],
	ssuffix = ".setAttribute",
	targets = {},
	itargets = {},
	ntargets = {};
	targets.domtargets = {
		"addEventListener": prefs,
		"addEventListener": [""],
		"$(window).on": prefs,
		"$(this).on": prefs,
		"$(self).on": prefs,
		"$(top).on": prefs,
		"$(window).one": prefs,
		"$(this).one": prefs,
		"$(self).one": prefs,
		"$(top).one": prefs
		//"$(global).on": prefs,
	}
	targets.domtargets2 = {
		"onmessage": prefs
	}
	targets.etargets = {
		"chrome.runtime.onMessage.addListener": prefs,
		"browser.runtime.onMessage.addListener": prefs,
		"runtime.onMessage.addListener": [""],
		"onMessage.addListener": [""],
		"chrome.extension.onRequest.addListener": prefs,
		"browser.extension.onRequest.addListener": prefs,
		"extension.onRequest.addListener": [""],
		"onRequest.addListener": [""]

	}
	targets.etargets_c = {
		"chrome.runtime.onConnect.addListener": prefs,
		"browser.runtime.onConnect.addListener": prefs,
		"runtime.onConnect.addListener": [""],
		"onConnect.addListener": [""] 
	}
	targets.ctargets = { // Get the port names and now lookup onMessage, add EventListener
		"chrome.runtime.connect": prefs,
		"browser.runtime.connect": prefs,
		"runtime.connect": [""]
	}
	targets.extargets = {
		"chrome.runtime.onMessageExternal.addListener": prefs,
		"browser.runtime.onMessageExternal.addListener": prefs,
		"runtime.onMessageExternal.addListener": [""],
		"onMessageExternal.addListener": [""],
		"onMessageExternal": []
	}
	targets.extargets_c = {
		"chrome.runtime.onConnectExternal.addListener": prefs,
		"browser.runtime.onConnectExternal.addListener": prefs,
		"runtime.onConnectExternal.addListener": [""],
		"onConnectExternal.addListener": [""],
		"onConnectExternal": []
	}
	targets.tabs = {
		"chrome.tabs.executeScript": prefs,
		"browser.tabs.executeScript": prefs,
		"tabs.executeScript": [""],
		"executeScript": []
	}/*
	targets.tabs2 = {
		"chrome.tabs.executeScript": [],
		"browser.tabs.executeScript": [],
	}*/
	targets.postMessage = {
		"postMessage": prefs,
		"source.postMessage": [""],
		"port.postMessage": []
	}
	targets.postMessages = {
		"postMessage": [""],
		"source.postMessage": [""]
	}
	targets.sendMessage = {
		"tabs.sendMessage": [""],
		"runtime.sendMessage": [""],
		"chrome.tabs.sendMessage": prefs,
		"chrome.runtime.sendMessage": prefs,
		"browser.runtime.sendMessage": prefs,
		"browser.tabs.sendMessage": prefs,
		"sendMessage": [""],
		"chrome.extension.sendRequest": prefs,
		"browser.extension.sendRequest": prefs,
		"extension.sendRequest": [""],
		"sendRequest": [""]
	}
	targets.appendChild = {
		"appendChild": [""],
		"append": [""]
	}
	targets.append = { // jQuery append method
		"append": [""]
	}
	targets.injection = {
		"append": [""],
		"appendChild": [""],
		"replaceChild": [""],
		"prepend": [""],
		"insertBefore": [""],
		//"insertAdjacentElement": [""],
		//"insertAdjacentHTML": [""],
		//"html": [""]
	}
	targets.attributesAssign = {
		"src": [""], //img, iframe, embed, script, poster, source
		"href": [""], // link, a
		"data": [""] // object
	}
	targets.attributesSet = {
		"setAttribute": [""],
		"attr": [""],
	}
	targets.getURL = {
		"chrome.runtime.getURL": prefs,
		"browser.runtime.getURL": prefs,
		"browser.extension.getURL": prefs,
		"chrome.extension.getURL": prefs,
		"runtime.getURL": [""],
		"extension.getURL": [""]
	}
	targets.requests = {
		"chrome.webRequest.onBeforeRequest.addListener": prefs,
		"browser.webRequest.onBeforeRequest.addListener": prefs,
		"chrome.webRequest.onBeforeSendHeaders.addListener": prefs,
		"browser.webRequest.onBeforeSendHeaders.addListener": prefs,
		"webRequest.onBeforeRequest.addListener": [""],
		"onBeforeRequest.addListener": [""],
		"webRequest.onBeforeSendHeaders.addListener": [""],
		"onBeforeSendHeaders.addListener": [""]
	}
	targets.responses = {
		"chrome.webRequest.onHeadersReceived.addListener": prefs,
		"browser.webRequest.onHeadersReceived.addListener": prefs,
		"webRequest.onHeadersReceived.addListener": [""],
		"onHeadersReceived.addListener": [""]
	}
	targets.ajax = {
		"fetch": prefs,
		"$.get": prefs,
		"$$.get": prefs,
		"jQuery.get": prefs,
		"$.post": prefs,
		"$$.post": prefs,
		"jQuery.post": prefs,
		"$.ajax": prefs,
		"$$.ajax": prefs,
		"jQuery.ajax": prefs,
		"$.getJSON": prefs,
		"$$.getJSON": prefs,
		"jQuery.getJSON": prefs,
		"$http.get": prefs,
		"$http.post": prefs,
		"$http.jsonp": prefs,
		"open": [""]
	}
	targets.setData = {
		"chrome.storage.local.set": prefs,
		"browser.storage.sync.set": prefs,
		"storage.local.set": [""],
		"storage.sync.set": [""],
		"local.set": [""],
		"sync.set": [""]
	}
	targets.getData = {
		"chrome.storage.local.get": prefs,
		"browser.storage.sync.get": prefs,
		"storage.local.get": [""],
		"storage.sync.get": [""],
		"local.get": [""],
		"sync.get": [""]
	}
	targets.evals = {
		"eval": prefs,
		"setTimeout": prefs,
		"setInterval": prefs,
	}
	targets.management = {
		"chrome.management.getAll": prefs,
		"management.getAll": [""],
		"chrome.management.get": prefs,
		"management.get": [""],
		"chrome.management.getSelf": prefs,
		"management.getSelf": [""],
		"chrome.management.getPermissionWarningsById": prefs,
		"management.getPermissionWarningsById": [""],
		"chrome.management.getPermissionWarningsByManifest": prefs,
		"management.getPermissionWarningsByManifest": [""],
		"chrome.management.setEnabled": prefs,
		"management.setEnabled": [""],
		"chrome.management.uninstall": prefs,
		"management.uninstall": [""],
		"chrome.management.uninstallSelf": prefs,
		"management.uninstallSelf": [""],
		"chrome.management.launchApp": prefs,
		"management.launchApp": [""],
		"chrome.management.createAppShortcut": prefs,
		"management.createAppShortcut": [""],
		"chrome.management.setLaunchType": prefs,
		"management.setLaunchType": [""],
		"chrome.management.generateAppForLink": prefs,
		"management.generateAppForLink": [""],
		"browser.management.getAll": prefs,
		"browser.management.get": prefs,
		"browser.management.getSelf": prefs,
		"browser.management.getPermissionWarningsById": prefs,
		"browser.management.getPermissionWarningsByManifest": prefs,
		"browser.management.setEnabled": prefs,
		"browser.management.uninstall": prefs,
		"browser.management.uninstallSelf": prefs,
		"browser.management.launchApp": prefs,
		"browser.management.createAppShortcut": prefs,
		"browser.management.setLaunchType": prefs,
		"browser.management.generateAppForLink": prefs,
	}

	targets.history = {
		"chrome.history.search": prefs,
		"chrome.history.getVisits": prefs,
		"chrome.history.addUrl": prefs,
		"chrome.history.deleteUrl": prefs,
		"chrome.history.deleteRange": prefs,
		"chrome.history.deleteAll": prefs,
		"browser.history.search": prefs,
		"browser.history.getVisits": prefs,
		"browser.history.addUrl": prefs,
		"browser.history.deleteUrl": prefs,
		"browser.history.deleteRange": prefs,
		"browser.history.deleteAll": prefs,
		"history.search": [""],
		"history.getVisits": [""],
		"history.addUrl": [""],
		"history.deleteUrl": [""],
		"history.deleteRange": [""],
		"history.deleteAll": [""]
	}
	targets.bookmarks = {
		"chrome.bookmarks.get": prefs,
		"chrome.bookmarks.getChildren": prefs,
		"chrome.bookmarks.getRecent": prefs,
		"chrome.bookmarks.getTree": prefs,
		"chrome.bookmarks.getSubTree": prefs,
		"chrome.bookmarks.search": prefs,
		"chrome.bookmarks.create": prefs,
		"chrome.bookmarks.move": prefs,
		"chrome.bookmarks.update": prefs,
		"chrome.bookmarks.remove": prefs,
		"chrome.bookmarks.removeTree": prefs,
		"browser.bookmarks.get": prefs,
		"browser.bookmarks.getChildren": prefs,
		"browser.bookmarks.getRecent": prefs,
		"browser.bookmarks.getTree": prefs,
		"browser.bookmarks.getSubTree": prefs,
		"browser.bookmarks.search": prefs,
		"browser.bookmarks.create": prefs,
		"browser.bookmarks.move": prefs,
		"browser.bookmarks.update": prefs,
		"browser.bookmarks.remove": prefs,
		"browser.bookmarks.removeTree": prefs,
		"bookmarks.get": [""],
		"bookmarks.getChildren": [""],
		"bookmarks.getRecent": [""],
		"bookmarks.getTree": [""],
		"bookmarks.getSubTree": [""],
		"bookmarks.search": [""],
		"bookmarks.create": [""],
		"bookmarks.move": [""],
		"bookmarks.update": [""],
		"bookmarks.remove": [""],
		"bookmarks.removeTree": [""]
	}
	targets.browsingData = {
		"chrome.browsingData.settings": prefs,
		"chrome.browsingData.remove": prefs,
		"chrome.browsingData.removeAppcache": prefs,
		"chrome.browsingData.removeCache": prefs,
		"chrome.browsingData.removeCookies": prefs,
		"chrome.browsingData.removeDownloads": prefs,
		"chrome.browsingData.removeFileSystems": prefs,
		"chrome.browsingData.removeFormData": prefs,
		"chrome.browsingData.removeHistory": prefs,
		"chrome.browsingData.removeIndexedDB": prefs,
		"chrome.browsingData.removeLocalStorage": prefs,
		"chrome.browsingData.removePluginData": prefs,
		"chrome.browsingData.removePasswords": prefs,
		"chrome.browsingData.removeWebSQL": prefs,
		"browser.browsingData.settings": prefs,
		"browser.browsingData.remove": prefs,
		"browser.browsingData.removeAppcache": prefs,
		"browser.browsingData.removeCache": prefs,
		"browser.browsingData.removeCookies": prefs,
		"browser.browsingData.removeDownloads": prefs,
		"browser.browsingData.removeFileSystems": prefs,
		"browser.browsingData.removeFormData": prefs,
		"browser.browsingData.removeHistory": prefs,
		"browser.browsingData.removeIndexedDB": prefs,
		"browser.browsingData.removeLocalStorage": prefs,
		"browser.browsingData.removePluginData": prefs,
		"browser.browsingData.removePasswords": prefs,
		"browser.browsingData.removeWebSQL": prefs,
		"browsingData.settings": [""],
		"browsingData.remove": [""],
		"browsingData.removeAppcache": [""],
		"browsingData.removeCache": [""],
		"browsingData.removeCookies": [""],
		"browsingData.removeDownloads": [""],
		"browsingData.removeFileSystems": [""],
		"browsingData.removeFormData": [""],
		"browsingData.removeHistory": [""],
		"browsingData.removeIndexedDB": [""],
		"browsingData.removeLocalStorage": [""],
		"browsingData.removePluginData": [""],
		"browsingData.removePasswords": [""],
		"browsingData.removeWebSQL": [""],
	}
	targets.cookies = {
		"chrome.cookies.get": prefs,
		"chrome.cookies.getAll": prefs,
		"chrome.cookies.set": prefs,
		"chrome.cookies.remove": prefs,
		"chrome.cookies.getAllCookieStores": prefs,
		"browser.cookies.get": prefs,
		"browser.cookies.getAll": prefs,
		"browser.cookies.set": prefs,
		"browser.cookies.remove": prefs,
		"browser.cookies.getAllCookieStores": prefs,
		"cookies.get": [""],
		"cookies.getAll": [""],
		"cookies.set": [""],
		"cookies.remove": [""],
		"cookies.getAllCookieStores": [""],
	}
	targets.downloads = {
		"chrome.downloads.download": prefs,
		"chrome.downloads.search": prefs,
		"chrome.downloads.pause": prefs,
		"chrome.downloads.resume": prefs,
		"chrome.downloads.cancel": prefs,
		"chrome.downloads.getFileIcon": prefs,
		"chrome.downloads.open": prefs,
		"chrome.downloads.show": prefs,
		"chrome.downloads.showDefaultFolder": prefs,
		"chrome.downloads.erase": prefs,
		"chrome.downloads.removeFile": prefs,
		"chrome.downloads.acceptDanger": prefs,
		"chrome.downloads.drag": prefs,
		"chrome.downloads.setShelfEnabled": prefs,
		"browser.downloads.download": prefs,
		"browser.downloads.search": prefs,
		"browser.downloads.pause": prefs,
		"browser.downloads.resume": prefs,
		"browser.downloads.cancel": prefs,
		"browser.downloads.getFileIcon": prefs,
		"browser.downloads.open": prefs,
		"browser.downloads.show": prefs,
		"browser.downloads.showDefaultFolder": prefs,
		"browser.downloads.erase": prefs,
		"browser.downloads.removeFile": prefs,
		"browser.downloads.acceptDanger": prefs,
		"browser.downloads.drag": prefs,
		"browser.downloads.setShelfEnabled": prefs,
		"downloads.download": [""],
		"downloads.search": [""],
		"downloads.pause": [""],
		"downloads.resume": [""],
		"downloads.cancel": [""],
		"downloads.getFileIcon": [""],
		"downloads.open": [""],
		"downloads.show": [""],
		"downloads.showDefaultFolder": [""],
		"downloads.erase": [""],
		"downloads.removeFile": [""],
		"downloads.acceptDanger": [""],
		"downloads.drag": [""],
		"downloads.setShelfEnabled": [""]
	}
	targets.topSites = {
		"topSites.get": [""],
		"chrome.topSites.get": prefs,
		"browser.topSites.get": prefs
	}
	targets.localStorage = {
		"localStorage.setItem": prefs,
		"sessionStorage.setItem": prefs
	}
	itargets.localStorage = ["localStorage", "sessionStorage", "setItem"]
	itargets.getURL = []
	itargets.ajax = [
		"send", "open", "onreadystatechange",
		"readyState", "status", "responseText",
		"responseXML", "response", "onload", "success", 
		"GET", "POST", "data", "done",
		"XMLHttpRequest", "ActiveXObject", "setRequestHeader", "withCredentials"]
	itargets.setData = ["storage", "local", "sync", "set"]
	itargets.getData = ["storage", "local", "sync", "get"]
	itargets.evals = ["eval", "setTimeout", "setInterval"]
	itargets.postMessages = ["postMessage", "source"]
	itargets.postMessage = ["postMessage", "source"]
	itargets.sendMessage = ["sendMessage", "sendRequest"]
	itargets.management = [
		"get", "getAll", "getSelf", "management", "IconInfo", "LaunchType", "ExtensionDisabledReason",
		"getPermissionWarningsById", "uninstall", "ExtensionType", "ExtensionInstallType", "ExtensionInfo",
		"getPermissionWarningsByManifest", "launchApp",
		"setEnabled", "createAppShortcut", "setLaunchType",
		"generateAppForLink", "uninstallSelf"
	]
	itargets.history = [
		"history", "search", "getVisits", "addUrl", "VisitItem", "onVisited", "onVisitRemoved",
		"deleteUrl", "deleteRange", "deleteAll", "TransitionType", "HistoryItem"
	]
	itargets.bookmarks = [
		"bookmarks", "get", "getChildren", "getRecent",
		"getTree", "getSubTree", "search", "create", "move", "onImportBegan", "onImportEnded",
		"update", "remove", "removeTree", "BookmarkTreeNodeUnmodifiable", "BookmarkTreeNode", "onChildrenReordered"
	]
	itargets.browsingData = [
		"browsingData", "settings", "remove", 
		"removeAppcache", "removeCache", "removeCookies",
		"removeDownloads", "removeFileSystems", 
		"removeFormData", "removeHistory", "removeIndexedDB",
		"removeLocalStorage", "removePluginData", 
		"removePasswords", "removeWebSQL", "RemovalOptions", "DataTypeSet"
	]
	itargets.cookies = [
		"getAllCookieStores", "cookies", "SameSiteStatus", "CookieStore", "OnChangedCause"
	]
	itargets.downloads = [
		"download","search","pause", "resume", 
		"cancel", "getFileIcon", "open", "show", "onDeterminingFilename", 
		"showDefaultFolder", "erase", "removeFile", "DownloadItem", "StringDelta", "BooleanDelta",
		"acceptDanger", "drag", "setShelfEnabled", "FilenameConflictAction", "InterruptReason", "DangerType"
	]
	itargets.tabs = [
		"executeScript"
	]
	itargets.topSites = ["MostVisitedURL"]
	/*itargets.requests = [
		"Origin", "origin", "ORIGIN",
		"Access-Control-Request-Method", "Access-control-request-method", "access-control-request-method", "ACCESS-CONTROL-REQUEST-METHOD",
		"Access-Control-Request-Headers", "Access-control-request-headers", "access-control-request-headers", "ACCESS-CONTROL-REQUEST-HEADERS",
	]
	itargets.responses = [
		"Access-Control-Allow-Origin", "Allow-control-allow-origin", "allow-control-allow-origin", "ALLOW-CONTROL-ALLOW-ORIGIN",
		"Access-Control-Allow-Methods", "Access-control-allow-methods", "access-control-allow-methods", "ACCESS-CONTROL-ALLOW-METHODS",
		"Access-Control-Allow-Headers", "Access-control-allow-headers", "access-control-allow-headers", "ACCESS-CONTROL-ALLOW-HEADERS",
		"Access-Control-Allow-Credentials", "Access-control-allow-credentials", "access-control-allow-credentials", "ACCESS-CONTROL-ALLOW-CREDENTIALS",
		"Access-Control-Expose-Headers", "Access-control-expose-headers", "access-control-expose-headers", "ACCESS-CONTROL-EXPOSE-HEADERS"
	]*/
	itargets.requests = [
		"origin", "Access-Control-Request-Method", "Access-Control-Request-Headers"
	]
	itargets.responses = [
		"Access-Control-Allow-Origin", "Access-Control-Allow-Methods", 
		"Access-Control-Allow-Headers", "Access-Control-Allow-Credentials", "Access-Control-Expose-Headers",
		"X-Frame-Options", "Frame-Options", "Content-Security-Policy", "Content-Security-Policy-Report-Only",
		"X-Content-Security-Policy", "X-Content-Security-Policy-Report-Only", "X-Webkit-CSP"
	]
	
function getTargets(){
	let genTargets = {},
		gnbargs = {domtargets: 2}
	for(let key in targets){
		genTargets[key] = {}
		for(let tkey in targets[key]){
			if(!(tkey in genTargets[key]))
				genTargets[key][tkey] = []
			let res = generateTargets(tkey, targets[key][tkey], checkPref, gnbargs[key] || 1)
			genTargets[key][tkey] = genTargets[key][tkey].concat(res)
			if(targets[key][tkey].length == 0){
				let skey = tkey.split(".")
				if(skey.length > 2){
					res = generateTargets(tkey, targets[key][tkey], !checkPref, gnbargs[key] || 1)
					genTargets[key][tkey] = genTargets[key][tkey].concat(res)
				}
			}
		}
	}
	return genTargets
}

	//Generate dynamic targets targets
function dynamicTargets(tname, prefs){
	return generateTargets(tname, prefs, false,  1)
}

	//Generate Targets check Arguments
function targetsCheckPrefNotIndex(tnames){
	let results = {}
	for(let tn in tnames){
		results["." + tn] = tnames[tn]
		results['["' + tn + '"]'] = tnames[tn]
	}
	return results
}



	//Transform array of targets to objects...
function arrayTargetsToObject(targets){
	let otargets = {}
	for(let t=0; t<targets.length; t++){
		otargets[targets[t].tname] = ""
	}
	return otargets
}

	//Prepare Targets
function prepareTargets(){

	let genTargets = getTargets()
	let results = {}
	for(let key in genTargets){
		results[key] = {}
		for(let tkey in genTargets[key]){
			results[key][tkey] = genTargets[key][tkey]
		}
	}
	let fresults = {}
	for(let key in results){
		fresults[key] = []
		for(let tkey in results[key]){
			fresults[key] = fresults[key].concat(results[key][tkey])
		}
	}
	return fresults
}

module.exports = {
	getTargets: getTargets,
	dynamicTargets: dynamicTargets,
	prepareTargets: prepareTargets,
	prefs: prefs,
	esuffix: esuffix,
	psuffix: psuffix,
	ssuffix: ssuffix,
	asuffixes: asuffixes,
	srcsuffix: srcsuffix,
	itargets: itargets,
	arrayTargetsToObject: arrayTargetsToObject,
	targetsCheckPrefNotIndex: targetsCheckPrefNotIndex
}
