/*
  
A tool for analyzing the message passing APIs 
between browser extensions and web applications
   
   Copyright (C) 2017 - 2019 INRIA
                                                                     
    Bug descriptions, user reports, comments or suggestions are   
    welcome. Send them to doliere.some@inria.fr or some.frncs@gmail.com
                                   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or 
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,   
   but WITHOUT ANY WARRANTY; without even the implied warranty of    
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     
   GNU General Public License for more details.                      
                                                                     
   You should have received a copy of the GNU General Public         
   License along with this program; if not, write to the Free        
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,   
   MA 02111-1307, USA.  

*/
// =======================================================================================
//    analyzer.js
//    ------------------------------------------------------------------------------------           
//    Author      :  Dolière Francis Somé                                    
//    Creation    :  Monday January 21 11:04:40 2019                          
//    Last change :  Monday January 21 11:04:40 2019               
//    Copyright   :  2017-2019 Dolière Francis Somé                          
// =======================================================================================


const fs = require('fs'),
	path = require('path'),
	helper = require('./helper'),
	reader = require('./reader'),
	astvisit = require('./astvisit'),
	targetter = require('./targets');


var reskeys = ["respond", "evals", "setData", "getData", "ajax", 
			"management", "history", "bookmarks", "browsingData", 
			"cookies", "downloads", "tabs", "topSites"];

	//Get CLI Arguments
function getArguments(){
	try{
		const browser = process.argv[2],
		indir = process.argv[3],
		outdir = process.argv[4];
		return [indir, outdir, browser];
	}catch(e){}
	return []
}

	//Analyse extension
extensionAnalyser()

function extensionAnalyser(){
	var [indir, outdir, browser] = getArguments(),
		 manifest = reader.parseManifest(indir),
		infos = reader.getManifestInfo(manifest, browser),
		eres = analyzeExtension(indir, manifest, infos);
	//if(Object.keys(eres).length)
	writeFile(outdir, JSON.stringify(eres))
}

function analyzeExtension(prefix, data, infos){
	//Parse Data
	var [bcodes, bscripts] = reader.getBackData(prefix, data),
		[uicodes, uiscripts] = reader.getUIData(prefix, data),
		[cscodes, cscripts] = reader.getContentScripts(prefix, data),
		econstructs = {}, csc = [], dcsc = {}, dcfiles = {}

		//Extension pages codes
	if (bcodes){
		var lconstructs = astvisit.parseAndVisit(bcodes)
		if(lconstructs){
			econstructs.back = lconstructs
		}
	}
		// UI pages codes
	for(var key in uicodes){
		if(uicodes[key]){
			var lconstructs = astvisit.parseAndVisit(uicodes[key])
			if (lconstructs){
				econstructs[key] = lconstructs
			}
		}
	}
		//Get/parse dynamically injected content scripts
	for(var key in econstructs){
		var [dc, df] = helper.handleDynanicContentScripts(econstructs[key], prefix)
		dcsc[key] = dc
		dcfiles[key] = df;
	}
		//Parse Content Scripts
	if(cscodes){
		for(var c=0; c<cscodes.length; c++){
			var cconstructs = astvisit.parseAndVisit(cscodes[c])
			if (cconstructs){
				csc.push(cconstructs)
			}
		}
	}
	let rcs = analyzeContentScripts(csc, dcsc, econstructs, infos),
		rbs = anlyzeBackgroundUIPageMessages(econstructs, infos)
	return [rcs, rbs, dcfiles];
}

	//Analyze Content Scripts (including dynamically injected ones)
function analyzeContentScripts(csc, dcsc, econstructs, infos){

		var sendToBack = false,
			fresults = {
				cs: {},
				dcs: {},
				pages: {}
			}
			//Analyze Content Scripts
		for(var c=0; c<csc.length; c++){
			let res = analyzePointersAndHandlers(csc[c], infos)
			if(res[6]){
				sendToBack = true
			}
			//console.log("CRES ", res[0])
			fresults.cs[c] = {
				pmsg: res[0],
				emsg: res[1]
			}
		}

			//Analyze Dynamic content scripts
		for(let key in dcsc){ // back, ui page
			let dc = dcsc[key]
			fresults.dcs[key] = {}

			for(let fc in dc){ // file or code
				fresults.dcs[key][fc] = {}
				for(let d=0; d<dc[fc].length; d++){
					let res = analyzePointersAndHandlers(dc[fc][d], infos)
					if(res[6])
						sendToBack = true
					//console.log("DRES ", res[0])
					fresults.dcs[key][fc][d] = {
						pmsg: res[0],
						emsg: res[1]
					}
				}

			}
		}
			//Analyze background and ui messages
		if(sendToBack){
			//Analyze background and UI
			for (let key in econstructs){
				let [bH, bM] = helper.extensionMessages(econstructs[key]),
					[shandlers, spointers, sparams] = helper.analyzeHandlers(bH, econstructs[key]),
					bres = getEvalXHRSetGetDataMatches(spointers, shandlers, null, infos)
				fresults.pages[key] = bres
			}
		}
		
		return fresults
}
	//Get Eval, XHR, Set Data, Get Data
function analyzePointersAndHandlers(constructs, infos){
	var [cH, cM] = helper.pageMessagesCScripts(constructs),
		[handlers, pointers, params] = helper.analyzeHandlers(cH, constructs),
		sMCalls = helper.extensionSendMessagesCalls(constructs, pointers) // Send Message Calls
		//Check whether any of the pointers matches a send message...
		//This includes ports, and normal messages.
		//Then, analyze message listeners in background, ui page, and content script
		resu = getEvalXHRSetGetDataMatches(pointers, handlers, constructs, infos),
		sresults = {}

		if(Object.keys(sMCalls).length > 0){
			var [sH, sM] = helper.extensionMessages(constructs),
			[shandlers, spointers, sparams] = helper.analyzeHandlers(sH, constructs)
			sresults = getEvalXHRSetGetDataMatches(spointers, shandlers, null, infos)
		}

		//Get Injected content URLs
	return [resu, sresults, handlers, pointers, params, cM, Object.keys(sMCalls).length > 0]
}

	//Get Eval, XHR, Set Data, Get Data
function getEvalXHRSetGetDataMatches(pointers, handlers, constructs, infos){
	let results = {};
	for(let r=0; r<reskeys.length; r++){
		if(reskeys[r] != "respond")
			results[reskeys[r]] = helper.getMatches(pointers, handlers, reskeys[r]);
	}
	return results
}





	//Analyze Background and UI pages
function anlyzeBackgroundUIPageMessages(constructs, infos){
		//Parse Data
	if(infos.externally_connectable){
		let results = {};

		for(let r=0; r<reskeys.length; r++)
			results[reskeys[r]] = {};

			//External messages
		if(infos.externally_connectable){
			for(let key in constructs){
				if(!constructs[key]){
					continue;
				}
				let [cH, cM] = helper.pageMessagesBack(constructs[key]),
					[handlers, pointers, params] = helper.analyzeHandlers(cH, constructs[key]);
				
					//Responding to message ?
				for(let pkey in params){
					if(params[pkey] && 
						params[pkey].length >= 3 && 
						(pkey in pointers && params[pkey][2] in pointers[pkey])){
						if(!(key in results.respond)){
							results.respond[key] = [];
						}
						results.respond[key].push(params[pkey][2]);
					}
				}
				for(let rkey in results){
					if(rkey != "respond")
						results[rkey][key] = helper.getMatches(pointers, handlers, rkey);
				}
			}
		}
		return results;
	}
	return {};
}



function writeFile(pth, data){
	//console.log("PATH ", pth, "DATA ", data)
	fs.writeFile(pth, data, (err) => {
	  //if (err) throw err;
	});
}
