/*
  
A tool for analyzing the message passing APIs 
between browser extensions and web applications
   
   Copyright (C) 2017 - 2019 INRIA
                                                                     
    Bug descriptions, user reports, comments or suggestions are   
    welcome. Send them to doliere.some@inria.fr or some.frncs@gmail.com
                                   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or 
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,   
   but WITHOUT ANY WARRANTY; without even the implied warranty of    
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     
   GNU General Public License for more details.                      
                                                                     
   You should have received a copy of the GNU General Public         
   License along with this program; if not, write to the Free        
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,   
   MA 02111-1307, USA.  

*/
// =======================================================================================
//    astvisit.js
//    ------------------------------------------------------------------------------------           
//    Author      :  Dolière Francis Somé                                    
//    Creation    :  Monday January 21 11:04:40 2019                          
//    Last change :  Monday January 21 11:04:40 2019               
//    Copyright   :  2017-2019 Dolière Francis Somé                          
// =======================================================================================


const recast = require('recast'),	
	asthandler = require('./asthandler')
var visits = {},
	ignoreProps = ["constructor", "hasOwnProperty", "toString", "isPrototypeOf"]


visits.AssignmentExpression = function(results, path){
	try{
		let left = path.node.left,
			right = path.node.right
		if(!(left.type in results))
			results[left.type] = {}
		if(!(right.type in results[left.type]))
			results[left.type][right.type] = []
		if(results[left.type][right.type].indexOf(path.node) == -1)
			results[left.type][right.type].push(path.node)
	}catch(e){}
	this.traverse(path)
}	
	//Variable declaractions var x = b
visits.VariableDeclaration = function(results, path){
	try{
		let decs = path.node.declarations
		for(let d=0; d<decs.length; d++){
			let dec = decs[d]
			if(dec.type == "VariableDeclarator"){
				let id = dec.id,
					init = dec.init
				if(!(id.name in results))
					results[id.name] = []
				results[id.name].push(init)
			}
		}
	}catch(e){}
	this.traverse(path)
}


visits.VariableDeclarator = function(results, path){
	try{
		let dec = path.node
		let id = dec.id,
			init = dec.init
		if(ignoreProps.indexOf(id.name) == -1 && init){
			if(!(id.name in results))
				results[id.name] = []
			results[id.name].push(init)
		}
	}catch(e){}
	this.traverse(path)
}
	//Gather function declarations function x(){}
visits.FunctionDeclaration = function(results, path){
	try{
		if(ignoreProps.indexOf(path.node.id.name) == -1){

			if(!(path.node.id.name in results))
				results[path.node.id.name] = []
			if(results[path.node.id.name].indexOf(path.node) == -1)
				results[path.node.id.name].push(path.node)
		}
	}catch(e){}
	this.traverse(path)
}
	
	// func()
visits.CallExpression = function(results, path){
	try{
		let tcallee = path.node.callee.type
		if(!(tcallee in results))
			results[tcallee] = []
		if(results[tcallee].indexOf(path.node) == -1)
			results[tcallee].push(path.node)
	}catch(e){}
	this.traverse(path)
}
	
	// new XHR()
visits.NewExpression = function(results, path){
	try{
		let tcallee = path.node.callee.type
		if(!(tcallee in results))
			results[tcallee] = []
		if(results[tcallee].indexOf(path.node) == -1)
			results[tcallee].push(path.node)
	}catch(e){}
	this.traverse(path)
}
	//Gather literals
visits.Literal = function(results, path){
	try{
		if(ignoreProps.indexOf(path.node.value) == -1){
			if(!(path.node.value in results))
				results[path.node.value] = 0
			results[path.node.value] ++
		}
	}catch(e){}
	this.traverse(path)
}
	// Gather Identifiers
visits.Identifier = function(results, path){
	try{
		if(ignoreProps.indexOf(path.node.name) == -1){
			if(!(path.node.name in results))
				results[path.node.name] = 0
			results[path.node.name] ++
		}
	}catch(e){}
	this.traverse(path)
}
	//Object Expressions
visits.ObjectExpression = function(results, path){
	try{
		for(let p=0; p<path.node.properties.length; p++){
			let prop = path.node.properties[p],
				pkey = prop.key.type == "Literal" ? prop.key.value : prop.key.name,
				pval = prop.value
				if (!(pval.type in results))
					results[pval.type] = []
				results[pval.type].push([pkey, pval])
		}
	}catch(e){}
	this.traverse(path)
}
	//Logical Expression
visits.LogicalExpression = function(results, path){
	try{
		let node = path.node,
			operator = node.operator,
			left = node.left,
			right = node.right

		if( !(operator in results))
			results[operator] = []//{}
		/*if (! (left.type in results[operator]) )
			results[operator][left.type] = {}
		if (! (right.type in results[operator][left.type]) )
			results[operator][left.type][right.type] = []
		//results[operator][left.type][right.type].push([node])
		//results[operator][left.type][right.type].push([left, right])*/

		results[operator].push(node)
	}catch(e){}
	this.traverse(path)
}

//Binary Expression
visits.BinaryExpression = visits.LogicalExpression


visits.MemberExpression = function(results, path){
	try{
		let mcast = recast.prettyPrint(path.node).code
		if(!(mcast in results))
			results[mcast] = 0;

		results[mcast] ++ //.push(path.node)
	}catch(e){}
	this.traverse(path)
}


function initiateVisits(ast){
	let results = {
		a: {}, // assignements
		v: {}, // variable declarations
		f: {}, // function definitions
		c: {}, // function calls
		n: {}, // new expressions
		l: {}, // literals
		i: {}, // identifiers
		o: {}, // Object expressions
		le: {}, // Logical expressions
		be: {}, // Binary expressions
		me: {} // Member Expressions
	}
	recast.visit(ast, {
		visitIdentifier: function(path){
			visits.Identifier.bind(this)(results.i, path)
		},
		visitLiteral: function(path){
			visits.Literal.bind(this)(results.l, path)
		},
		visitNewExpression: function(path){
			visits.NewExpression.bind(this)(results.n, path)
		},
		visitCallExpression: function(path){
			visits.CallExpression.bind(this)(results.c, path)
		},
		visitFunctionDeclaration: function(path){
			visits.FunctionDeclaration.bind(this)(results.f, path)
		}/*,
		visitVariableDeclaration: function(path){
			visits.VariableDeclaration.bind(this)(results.v, path)
		}*/,
		visitVariableDeclarator: function(path){
			visits.VariableDeclarator.bind(this)(results.v, path)
		},
		visitAssignmentExpression: function(path){
			visits.AssignmentExpression.bind(this)(results.a, path)
		},
		visitObjectExpression: function(path){
			visits.ObjectExpression.bind(this)(results.o, path)
		},
		visitLogicalExpression: function(path){
			visits.LogicalExpression.bind(this)(results.le, path)
		},
		visitBinaryExpression: function(path){
			visits.BinaryExpression.bind(this)(results.be, path)
		},
		visitMemberExpression: function(path){
			visits.MemberExpression.bind(this)(results.me, path)
		}
	});

		//Merge variable declarations with assignments...
	if(!(results.a.Identifier))
		results.a.Identifier = {}
	for(let id in results.v){
		for(let i=0; i<results.v[id].length; i++){
			let init = results.v[id][i]
			if(!(init.type in results.a.Identifier))
				results.a.Identifier[init.type] = []
			results.a.Identifier[init.type].push({
				type: "AssignmentExpression",
				operator: "=",
				left: {
                    type: "Identifier",
                    name: id
				},
				right: init
			});
		}
	}
		//Merge function declarations with assignments
	for(let id in results.f){
		for(let i=0; i<results.f[id].length; i++){
			let init = results.f[id][i]
			if(!(init.type in results.a.Identifier))
				results.a.Identifier[init.type] = []
			results.a.Identifier[init.type].push({
				type: "AssignmentExpression",
				operator: "=",
				left: {
                    type: "Identifier",
                    name: id
				},
				right: init
			});
		}
	}

	transformObjectExpressionsToAssignments(results)
	destructuringArrays(results)
	indexingAssignmentsCalls(results)
	let iresults = indexingAssignments(results),
		fresults = indexingFunctions(results),
		cresults = indexingFunctionCalls(results),
		aresults = indexingAssignmentFunctionCalls(results)

	results.als = iresults
	results.funcs = fresults
	results.calls = cresults[0],
	results.icalls  = cresults[1]
	results.acalls =  aresults

	return [results, iresults, fresults, cresults[0], cresults[1]]
	//return results
}

	//Get only function calls
function visitFunctionCalls(ast){
	let results = {}
	recast.visit(ast, {
		visitCallExpression: function(path){
			visits.CallExpression.bind(this)(results, path)
		}
	});

	return results
}
	
	//Transform object expressions into assignments.
function transformObjectExpressionsToAssignments(constructs){
	//
	let ltypes = {"Identifier": "", "MemberExpression": "", "ThisExpression": ""}

	for(let ltype in ltypes){
		if(constructs.a && constructs.a[ltype] && constructs.a[ltype]["ObjectExpression"]){
			let assigns = constructs.a[ltype].ObjectExpression
			for(let a=0; a<assigns.length; a++){
				let left = assigns[a].left,
					right = assigns[a].right,
					rproperties = right.properties,
					lcast = recast.prettyPrint(left).code
				for(let p=0; p<rproperties.length; p++){
					let pkey = rproperties[p].key,
						pval = rproperties[p].value,
						ikey, lkey // Literal and identifier versions of the key
					if(pkey.type == "Identifier"){
						ikey = {type: "Identifier", name: pkey.name}
						lkey = {type: "Literal", value: pkey.name, raw: '"' + pkey.name + '"'}
					}else if(pkey.type == "Literal"){
						ikey = {type: "Identifier", name: pkey.value}
						lkey = {type: "Literal", value: pkey.value, raw: pkey.raw}
					}
					addMemberExpression(constructs, left, ikey, pval, false)
					addMemberExpression(constructs, left, lkey, pval, true)
				}
			}

		}
	}
}
	//Destructing Arrays [a, b] = [c, d]
function destructuringArrays(constructs){
	if(constructs.a && constructs.a.ArrayPattern){
		//Array
		if(constructs.a.ArrayPattern.ArrayExpression){
			let assigns = constructs.a.ArrayPattern.ArrayExpression
			for(let a=0; a<assigns.length; a++){
				let left = assigns[a].left,
					right = assigns[a].right
				for(let l=0; l<left.elements.length; l++){
					if(right.elements.length > l){
						addAssignment(constructs, left.elements[l], right.elements[l])
					}else{
						break
					}
				}
			}
		}

			//CallExpressions Array()
		if(constructs.a.ArrayPattern.CallExpression){
			let assigns = constructs.a.ArrayPattern.CallExpression
			for(let a=0; a<assigns.length; a++){
				let left = assigns[a].left,
					right = assigns[a].right,
					rcallee = right.callee
				if(rcallee.type == "Identifier" && rcallee.name == "Array"){
					for(let l=0; l<left.elements.length; l++){
						if(right.arguments.length > l){
							addAssignment(constructs, left.elements[l], right.arguments[l])
						}else{
							break
						}
					}
				}
			}
		}
			//New Expressions new Array()
		if(constructs.a.ArrayPattern.NewExpression){
			let assigns = constructs.a.ArrayPattern.NewExpression
			for(let a=0; a<assigns.length; a++){
				let left = assigns[a].left,
					right = assigns[a].right,
					rcallee = right.callee
				if(rcallee.type == "Identifier" && rcallee.name == "Array"){
					for(let l=0; l<left.elements.length; l++){
						if(right.arguments.length > l){
							addAssignment(constructs, left.elements[l], right.arguments[l])
						}else{
							break
						}
					}
				}
			}
		}
	}
}

	//Add MemberExpression as an assignment
function addMemberExpression(constructs, left, pkey, pval, computed){
	if(!constructs.a)
		constructs.a = {}
	if(!constructs.a.MemberExpression){
		constructs.a.MemberExpression = {}
	}
	if(!constructs.a.MemberExpression[pval.type])
		constructs.a.MemberExpression[pval.type] = []
	let ass = {
            "type": "AssignmentExpression",
            "operator": "=",
            "left": {
                "type": "MemberExpression",
                "computed": computed,
                "object": left,
                "property": pkey
            },
            "right": pval
        }
    constructs.a.MemberExpression[pval.type].push(ass)
}

	//Function ADD to assignments
function addAssignment(constructs, left, right){
	if(!constructs.a)
		constructs.a = {}
	if(!constructs.a[left.type])
		constructs.a[left.type] = {}
	if(!constructs.a[left.type][right.type])
		constructs.a[left.type][right.type] = []
	constructs.a[left.type][right.type].push({
        "type": "AssignmentExpression",
        "operator": "=",
        "left": left,
        "right": right
    });
}




	//Indexing Assignments. Associate each left end side with its different values and reverse-aliases
function indexingAssignments(constructs){
	let types = {"Identifier": "", "MemberExpression": ""},
		results = {},
		aliases = {},
		faliases = {}
	/**
	*** Identifiers and Member expressions initially
	*** assigned to other identfiers and member expression
	*** x = z; x = t; ===> x : {z: "", t: ""}
	**/
	if(constructs.a){
		for(let ltype in types){
			if(constructs.a[ltype]){
					//Identifier and MemberExpression aliases
				for(let rtype in types){
					if(constructs.a[ltype][rtype]){
						let assigns = constructs.a[ltype][rtype]
						for(let a=0; a<assigns.length; a++){
							let left = assigns[a].left,
								right = assigns[a].right,
								rcast = recast.prettyPrint(right).code,
								lcast = recast.prettyPrint(left).code
								if( !((lcast + "") in aliases) ) {
									aliases[lcast] = {}
								}
								//console.log("LCAST ", lcast, assigns[a].left)
								aliases[lcast][rcast] = ""
						}
					}
				}
			}
		}
	}

	/**
	*** Recursively get all possible values of an
	*** identifier or member expression
	*** x = t, t = v, v = w.
	*** x = {t: "", v: "", w: ""}
	**/
	let changed = true,
		counter = 0
	while(changed){
		//counter ++
		changed = false
		for(let lcast in aliases){
			let nlcast = {}
			for(let rcast in aliases[lcast]){
				if(lcast != rcast){
					let [nchanged, nleft] = compareAddNew(aliases[lcast], aliases[rcast])
					if(nchanged){
						changed = true
					}
					nlcast = Object.assign({}, nlcast, nleft)
				}
			}
			aliases[lcast] = Object.assign({}, aliases[lcast], nlcast)
		}
	}

	/**
	*** Get other values associated to identifiers and
	*** member expressions, which are not identifiers or
	*** member expressions
	**/
	for(let ltype in types){
		for(let rtype in constructs.a[ltype]){
			if(!(rtype in types)){
				let assigns = constructs.a[ltype][rtype]
				for(let a=0; a<assigns.length; a++){
					let left = assigns[a].left,
						right = assigns[a].right,
						lcast = recast.prettyPrint(left).code
					if(!(lcast in results))
						results[lcast] = {}
					if(!(right.type in results[lcast]))
						results[lcast][right.type] = []
					results[lcast][right.type].push(right)
				}
			}
		}
	}
	constructs.aliases = aliases
	constructs.aindex = results

	//Final Results. Gather
	let fresults = {}
	for(let al in aliases){
		fresults[al] = [aliases[al], results[al]]
	}
		//Identifiers having no aliases
	for(let al in results){
		if(!(al in fresults))
			fresults[al] = [aliases[al], results[al]]
	}
	return fresults //aliases
}
	
	//Try to resolve some call expressions before starting
function indexingAssignmentsCalls(constructs){
	let types = {"Identifier": "", "MemberExpression": ""},
		resolved = []

	if(constructs.a){
		for(let ltype in types){
			if(constructs.a[ltype]){
				if(constructs.a[ltype].CallExpression){
					let assigns = constructs.a[ltype].CallExpression
					for(let a=0; a<assigns.length; a++){
						let left = assigns[a].left,
							right = assigns[a].right,
							lcast = recast.prettyPrint(left).code
						if(right.type == "CallExpression"){ // Always true
							//Check if is bound found...
							let res = asthandler.getFunctionNameAndArguments(right.callee, constructs)
							if(res && res[2]){
								res = res[0]
							}else if(right.callee.type == "FunctionExpression" ||
								right.callee.type == "FunctionDeclaration" || 
								right.callee.type == "ArrowFunctionExpression"){
								res = asthandler.resolveCallExpression(right, constructs)
								//console.log("HERE", res)						
							}
							if(res){
								resolved.push([left, res])
							}
						}
					}
				}
			}
		}
	}

	for(let r=0; r<resolved.length; r++){
		addAssignment(constructs, resolved[r][0], resolved[r][1])
	}
}

	//Indexing Assignments Function Expressions and definitions...
function indexingFunctions(constructs){
	//
	let types = {"Identifier": "", "MemberExpression": ""},
		ftypes = {
			"FunctionExpression": "", 
			"FunctionDeclaration": "",
			"ArrowFunctionExpression": ""},
		results = {},
		aliases = {}
	//Get aliases
	if(constructs.a){
		for(let ltype in types){
			if(constructs.a[ltype]){
				for(let rtype in ftypes){
					if(constructs.a[ltype][rtype]){
						let assigns = constructs.a[ltype][rtype]
						for(let a=0; a<assigns.length; a++){
							let left = assigns[a].left,
								right = assigns[a].right,
								lcast = recast.prettyPrint(left).code
							if(! (lcast in results))
								results[lcast] = []
							results[lcast].push(right)
						}
					}
				}
			}
		}
	}
	for(let func in results){
		let fres = visitFunctionCalls(results[func][0])
		for(let ctype in types){ // Identifer and member expression function calls only
			//Get function name and arguments...
			if(ctype in fres){
				for(let r=0; r<fres[ctype].length; r++){
					let right = fres[ctype][r]
					if(right.callee && 
						(!(right.callee.type in ftypes))) {
						let res = asthandler.getFunctionNameAndArguments(right.callee, right.arguments, constructs)
						if(res && !res[2]){ // Is not bound function
							let fcast = recast.prettyPrint(res[0]).code
							//Function name
							if(!(func in aliases))
								aliases[func] = {}
							aliases[func][fcast] = ""
						}
					}
				}
			}

		}
	}
	//Recursively get the list of other functions to call or analyse
	let changed = true,
		counter = 0
	while(changed){
		//counter ++
		changed = false
		for(let lcast in aliases){
			let nlcast = {}
			for(let rcast in aliases[lcast]){
				if(lcast != rcast){
					let [nchanged, nleft] = compareAddNew(aliases[lcast], aliases[rcast])
					if(nchanged){
						changed = true
					}
					nlcast = Object.assign({}, nlcast, nleft)
				}
			}
			aliases[lcast] = Object.assign({}, aliases[lcast], nlcast)
		}
	}
	constructs.ifuncs = aliases
	return results
}

	
	/**
	*** Indexing assignment function calls
	*** 
	**/
function indexingAssignmentFunctionCalls(constructs){
	let types = {"Identifier": "", "MemberExpression": ""},
		results = {},
		iresults = {},
		cons = {}
	if(constructs.a){
		for(let ltype in types){
			if(constructs.a[ltype] && constructs.a[ltype].CallExpression){
				let assigns = constructs.a[ltype].CallExpression
				for(let a=0; a<assigns.length; a++){
					let left = assigns[a].left,
						right = assigns[a].right,
						lcast = recast.prettyPrint(left).code,
						fcast = recast.prettyPrint(right.callee).code
					//console.log(right.type, "RTYPE", right.callee)
					if(right.callee && 
						["ArrowFunctionExpression", "FunctionExpression", "FunctionDeclaration"].indexOf(right.callee.type) == -1){
						let res = asthandler.getFunctionNameAndArguments(right.callee, right.arguments, constructs)
						if(res && !res[2]){ // Is not bound function
							try{
								let fccast = recast.prettyPrint(res[0]).code
								if(!(lcast in results))
									results[lcast] = {}
								if(! (fccast in results[lcast]))
									results[lcast][fccast] = {}
								if( !(fcast in results[lcast][fccast]))
									results[lcast][fccast][fcast] = []
								results[lcast][fccast][fcast].push(res)
							}catch(e){}

							/*if(! (fccast in iresults))
								iresults[fccast] = {}
							if( !(fcast in iresults[fccast]))
								iresults[fccast][fcast] = []
							iresults[fccast][fcast].push(res)*/
						}

					}
					/*if(!(lcast in cons))
						cons[lcast] = {}
					if(!(right.callee.type in cons[lcast]))
						cons[lcast][right.callee.type] = []
					cons[lcast][right.callee.type].push(right)*/
				}
				//
			}
		}
	}
	//console.log("acalls", results)
	return results
}

//Indexing Function Calls. Function names, and their arguments...
function indexingFunctionCalls(constructs){
	let results = {},
		iresults = {} // Inverse results
	if(constructs.c){
		for(let tcallee in constructs.c){
			if(tcallee != "FunctionExpression" && 
				tcallee != "FunctionDeclaration" &&
				tcallee != "ArrowFunctionExpression"){
				//Get callee and arguments...
				for(let c=0; c<constructs.c[tcallee].length; c++){
					let funcall = constructs.c[tcallee][c],
						res = asthandler.getFunctionNameAndArguments(funcall.callee, funcall.arguments, constructs),
						fcast = recast.prettyPrint(funcall.callee).code,
						fccast = recast.prettyPrint(res[0]).code
					try{
						if(!(fcast in results))
							results[fcast] = {}
						if(!(fccast in results[fcast]))
							results[fcast][fccast] = []
						//console.log("FCAST ", fcast, " FCCAST ", fccast)
						results[fcast][fccast].push(res)
							//Inverse results
						if(! (fccast in iresults))
							iresults[fccast] = {}
						if( !(fcast in iresults[fccast]))
							iresults[fccast][fcast] = []

						iresults[fccast][fcast].push(res)
					}catch(e){}
				}
			}
		}
	}
	return [results, iresults]
}


//Get Function Calls, from a certain function
function getFunctionCalls(constructs){
	let results = {},
		iresults = {} // Inverse results
	if(constructs.c){
		for(let tcallee in constructs.c){
			if(tcallee != "FunctionExpression" && 
				tcallee != "FunctionDeclaration" && 
				tcallee != "ArrowFunctionExpression"){
				//Get callee and arguments...
				for(let c=0; c<constructs.c[tcallee].length; c++){
					let funcall = constructs.c[tcallee][c],
						res = asthandler.getFunctionNameAndArguments(funcall.callee, funcall.arguments, constructs),
						fcast = recast.prettyPrint(funcall.callee).code,
						fccast = recast.prettyPrint(res[0]).code
					if(!(fcast in results))
						results[fcast] = {}
					if(!(fccast in results[fcast]))
						results[fcast][fccast] = []
					results[fcast][fccast].push(res)
						//Inverse results
					if(! (fccast in iresults))
						iresults[fccast] = {}
					if( !(fcast in iresults[fccast]))
						iresults[fccast][fcast] = []

					iresults[fccast][fcast].push(res)
				}
			}
		}
	}
	return [results, iresults]
}


	//Check if 2 objects are the same
function compareAddNew(left, right){
	let results = {},
		nlcast = Object.assign({}, left, right),
		changed = false
	for(let key in nlcast){
		if(!(key in left)){
			changed = true
			break
		}
	}
	return [changed, nlcast]
}


	//Parse code, and visit AST
function parseAndVisit(code){
	try{
		let ast = recast.parse(code)
		let ivis = initiateVisits(ast)
		//console.log("Returning")
		return ivis[0] // ivis
	}catch(e){
		console.log("Error ", e)
	}
	return null
}
		
	//Get function parameters
function getFunctionParamaters(func){
	let parameters = []
	if(func && func.params){
		for(let p=0; p<func.params.length; p++){
			let param = func.params[p]
			parameters.push(recast.prettyPrint(param).code)
		}
	}
	return parameters
}
	
	//Parse and return expression
function parseExpression(code){
	try{
		let ast = recast.parse("(" + code + ")")
		return ast.program.body[0].expression
	}catch(e){}
	return null
}
	
	//Parse Expressions
function parseExpressions(codes){
	let results = []
	for(let c=0; c<codes.length; c++){
		if(codes[c]){
			let ast = parseExpression(codes[c])
			if(ast)
				results.push(ast)
		}
	}
	return results
}

	//Remove duplicates
function uniq(a) {
    var seen = {};
    return a.filter(function(item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}

module.exports = {
	initiateVisits: initiateVisits,
	parseAndVisit: parseAndVisit,
	getFunctionParamaters: getFunctionParamaters,
	parseExpression: parseExpression,
	parseExpressions: parseExpressions
}