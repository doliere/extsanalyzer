/*
  
A tool for analyzing the message passing APIs 
between browser extensions and web applications
   
   Copyright (C) 2017 - 2019 INRIA
                                                                     
    Bug descriptions, user reports, comments or suggestions are   
    welcome. Send them to doliere.some@inria.fr or some.frncs@gmail.com
                                   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or 
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,   
   but WITHOUT ANY WARRANTY; without even the implied warranty of    
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     
   GNU General Public License for more details.                      
                                                                     
   You should have received a copy of the GNU General Public         
   License along with this program; if not, write to the Free        
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,   
   MA 02111-1307, USA.  

*/
// =======================================================================================
//    reader.js
//    ------------------------------------------------------------------------------------           
//    Author      :  Dolière Francis Somé                                    
//    Creation    :  Monday January 21 11:04:40 2019                          
//    Last change :  Monday January 21 11:04:40 2019               
//    Copyright   :  2017-2019 Dolière Francis Somé                          
// =======================================================================================



const fs = require('fs'),
	path = require('path'),
	rewrite = require('./rewrite'),
	astvisit = require('./astvisit')/*,
	uneval = require('../csp/unsafeeval')*/



	//Parse Manifest
function parseManifest(prefix){
	let fpath = prefix + "/manifest.json"
	if(fs.existsSync(fpath)){
		try{
			let data = fs.readFileSync(fpath);
			data = JSON.parse(data);
			return data;
		}catch(e){}
	}
	return {}
}

	//Get some information about the manifest
function getManifestInfo(data, browser){
	return {
		"externally_connectable": (("externally_connectable" in data) && (browser != "firefox")),
		"webRequest": (("permissions" in data) && (data["permissions"].indexOf("webRequest") >=0)),
		"unsafe_eval": hasUnsafeEval(data),
		"wars": (("web_accessible_resources" in data) && data["web_accessible_resources"] && data["web_accessible_resources"].length > 0),
		"browser": browser
	}
}

function getUserDataPermissions(data, perms){
	var res = []
	if(data["permissions"]){
		for(let p=0; p<perms.length; p++){
			if(data.permissions.indexOf(perms[p]) >=0)
				res.push(perms[p])
		}
	}
	return res.length > 0
}

function hasUnsafeEval(data){
	let csp = data.content_security_policy
	if(csp){
		let csps = removeSpaces(csp);
		csps = csps.split(",");
		for (let d=0; d<csps.length; d++){
			let policy = csps[d]
			if (!(policy == "")){
				policy = removeSpaces(policy);
				let dirs = getDirectives(policy),
					scripts = extractDirective(dirs, "script-src", "default-src");
				if(scripts && scripts.indexOf("'unsafe-eval'") >= 0){
					return true;
				}
			}
		}
	}
	return false;
}

function removeSpaces(data){
	let res = data.split(/\s+/).join(' ');
	res = res.replace(/^[ ]+|[ ]+$/g,'');
	return res;
}

function getDirectives(policy){
	policy = policy.split(";");
	let dirs = {}
	for(let p=0; p<policy.length; p++){
		let dir = policy[p].split(" "),	
			dname = dir[0],
			dvalues = dir.slice(1);
		if(!(dname in dirs))
			dirs[dname] = dvalues;
	}
	return dirs;
}

function extractDirective(policy, name, fb){
	return policy[name] || policy[fb] || []
}

	//Return background scripts data, and other information
function getBackData(prefix, data){
	let scripts = [], inlines = "";
	if("background" in data){
		if("page" in data["background"]){
	        let bpage = prefix + "/" + data["background"]["page"];
	        if(fs.existsSync(bpage)){
	          	let bpdata = readFileContent(bpage)
	          	let bscripts = rewrite.parse(bpdata.toString())

	          	scripts = gatherScripts(bscripts[0], prefix, data["background"]["page"])
	          	inlines = bscripts[1]
	        }
		}else  if("scripts" in data["background"]){
	        let bscripts = data["background"]["scripts"];
	        scripts = gatherScripts(bscripts, prefix, "")
		}

		let cscript = gatherScriptsContent(scripts); //
		return [cscript, scripts, (("permissions" in data) && 
		(data["permissions"].indexOf("tabs") >=0 || data["permissions"].indexOf("activeTab") >= 0)),
		("externally_connectable" in data), 
		(("permissions" in data) && 
		(data["permissions"].indexOf("webRequest") >=0))]
	}
	return []
}
	//Get UIs Data
function getUIData(prefix, data){
	let out = {},
	 	scripts = {},
		uis = ["browser_action", "page_action", "options_page", "options_ui"] // Recent ways of UIs
	
	for(let u=0; u<uis.length; u++){
		let entry = uis[u]
		if(entry in data){
			let uiname = ""
			if(entry == "options_page")
				uiname = data[entry]
			else if (entry == "options_ui")
				uiname = data[entry]["page"]
			else
				uiname = data[entry]["default_popup"]

			let upage = prefix + "/" + uiname
			upage = upage.split('?')
			upage = upage[0]
			upage = upage.split('#')
			upage = upage[0]
			if(fs.existsSync(upage)){
				let updata = readFileContent(upage)
				try{
					let uscripts = rewrite.parse(updata.toString())
					scripts[entry] = gatherScripts(uscripts[0], prefix, data[entry]["default_popup"])
				}catch(e){}
			}
		}
	}

	for(let entry in scripts){
		let cscript = gatherScriptsContent(scripts[entry]); //
		out[entry] = cscript
	}
	return [out, scripts]
}



	// Return content scripts, in different groups
function getContentScripts(prefix, data){
	let scripts = [], cscript = [];
	if("content_scripts" in data){
		let cscripts = data["content_scripts"]
		let nscripts = {};
		for (let c=0; c<cscripts.length; c++){
			let js = cscripts[c]["js"] || []
			let groupScripts = [] // different groups of scripts...
			for (let j=0; j<js.length; j++){
				if(js[j].indexOf("/") == 0)
					groupScripts.push(prefix + js[j]);
				else
					groupScripts.push(prefix+ "/" +js[j]);
			}
			//nscripts.push(groupScripts)
			scripts[c] = groupScripts
			cscript[c] = gatherScriptsContent(groupScripts)
		}
		return [cscript, scripts]
	}		
	return []
}
	//Gather dynamic scripts
function gatherDynamicScripts(scripts, prefix){
	let groupScripts = [] // different groups of scripts...
	for (let j=0; j<scripts.length; j++){
		try{
			if(scripts[j].indexOf("/") == 0)
				groupScripts.push(prefix + scripts[j]);
			else
				groupScripts.push(prefix+ "/" + scripts[j]);
		}catch(e){}
	}
	return gatherScriptsContent(groupScripts)
}


	//Gather all background scripts. Beware of the inline scripts in background pages
function gatherScripts(scripts, folder, pth){ 
  if(pth){
    pth = pth.split('/')
    pth = pth.slice(0, pth.length-1)
    if(pth.length > 0){
      pth = pth.join("/")
    }else{
      pth = ""
    }
  }else{
    pth = ""
  }

  let prefix  = folder
  if (pth){
    if(pth.indexOf("/") == 0)
      prefix = prefix + pth
    else
      prefix = prefix + "/" + pth
  }
  //console.log("Path " + prefix)

  let allScripts = []
  for(let s=0; s<scripts.length; s++){
    let script = scripts[s]
    if(script.indexOf("/") == 0){
      script = prefix + script
    }else{
      script = prefix + "/" + script
    }
    allScripts.push(script)
  }
  return allScripts
}

	//Gather all scripts in a single file. Content Script
function gatherScriptsContent(scripts){
	let allContents = []
	for(let s=0; s<scripts.length; s++){
		try{
			let data = fs.readFileSync(scripts[s]);
			if(data){
				allContents.push(data.toString())
			}
		}catch(e){}
	}
	return allContents.join("\n\n//------------ Script content separator -------------\n")
}


function readFileContent(pth){
  try{
    if(fs.existsSync(pth)){
      let data = fs.readFileSync(pth, "utf8")
      return data
    }
  }catch(e){
    return null
  }
}



module.exports = {
	parseManifest: parseManifest,
	getManifestInfo: getManifestInfo,
	getUserDataPermissions: getUserDataPermissions,
	getBackData: getBackData,
	getUIData: getUIData,
	getContentScripts: getContentScripts,
	gatherDynamicScripts: gatherDynamicScripts,
	gatherScriptsContent: gatherScriptsContent
}