# EmPoWeb
## A tool for analyzing communications (message passing) APIs between browser extensions and web applications

This is a command line (and complete) version of the online tool available at http://www-sop.inria.fr/members/Doliere.Some/empoweb/extsanalyzer/

# How to use it ?
First, you need to install Node.js and the NPM package <strong>jsdom</strong>. This package is used to parse and extracts scripts from extensions background and UI pages. 


Here is an example of commands to analyze a Chrome extension

<code>node analyzer.js chrome codes/myextension/ outputs/myextension.json</code>

<code>node results.js outputs/ chrome codes/ res.json</code>

where
<ul>
	<li><code>chrome</code> is the name of the browser in which you aim to publish the extension. The possible values are chrome, firefox or opera</li>
	<li><code>codes/myextension/</code> is a directory containing the source code of the extension to be analyzed. The analyzer will automatically read the <strong>manifest.json</strong> file of the extension in order to identify the different components of the extension to analyze, and their related files</li>
	<li><code>outputs/myextension.json</code> will contain the result of the analysis. It is important to ensure that the name of the extension in <code>ouputs/</code> directory matches that of the extension in the <strong>codes/</strong> directory</li>
	<li><code>res.json</code> contains the result of the analysis. In particular, it shows the extensions APIs that can be exploited by web applications. </li>
</ul>

Please take a look at the paper http://www-sop.inria.fr/members/Doliere.Some/papers/empoweb.pdf for more details about the manual analysis process. Also feel free to drop me an email if you want some help in reviewing your extension code


